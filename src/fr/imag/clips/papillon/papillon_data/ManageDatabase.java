/*-----------------------------------------------------------------------------
 * $Id: ManageDatabase.java 1064 2009-08-19 20:02:12Z mangeot $
 *-----------------------------------------------------------------------------
 * $Log$
 * Revision 1.9  2006/03/01 15:12:31  mangeot
 * Merge between maintrunk and LEXALP_1_1 branch
 *
 *-----------------------------------------------------------------------------
*/

package fr.imag.clips.papillon.papillon_data;

import fr.imag.clips.papillon.CurrentDBTransaction;
import fr.imag.clips.papillon.business.PapillonBusinessException;

import java.sql.*;

import com.lutris.appserver.server.*;
import com.lutris.appserver.server.sql.*;
import com.lutris.dods.builder.generator.query.DataObjectException;

import org.enhydra.dods.DODS;

/* For debug messages */
//import fr.imag.clips.papillon.business.PapillonLogger;

public class ManageDatabase implements Query {


    protected static final String createTableSql = "CREATE TABLE ";
    protected static final String createIndexSql = "CREATE INDEX ";
    protected static final String truncateTableSql = "TRUNCATE TABLE ";
    protected static final String dropTableSql = "DROP TABLE ";
    protected static final String dropIndexSql = "DROP INDEX ";

	protected static final String DatabaseUserString = "DatabaseManager.DB.papillon.Connection.User";

	protected static java.util.regex.Pattern quotePattern = java.util.regex.Pattern.compile("'");

    protected String currentSQL = "";
    protected DBTransaction transaction;

    /**
        * Public constructor
     */
    public ManageDatabase(DBTransaction trans, String sql) {
        this.transaction = trans;
        this.currentSQL = sql;
    }

   /**
        * WARNING!	 This method is	disabled.
     * The reason is that this special set of Queries do not return any result.
     * Moreover, it is also disabled in Quries implementations.
	 */
	public Object next(ResultSet rs) throws SQLException, ObjectIdException {
        // TODO: It	would be nice to throw an unchecked	exception here
        // (an exception that extends RuntimeException)
        // that	would be guaranteed	to appear during application testing.
        throw new ObjectIdException("next()	should not be used.	 Use getNextDO() instead." );
        //return null;
    }

    /*<table id="fr.imag.clips.papillon.data.VolumeEntry" dbTableName="volume">
        <column id="headword" isIndex="true" usedForQuery="true">
        <javadoc>
        </javadoc>
        <type canBeNull="true" dbType="LONGVARCHAR" javaType="String"/>
        </column>
        <column id="id" usedForQuery="true">
        <javadoc>
        </javadoc>
        <type canBeNull="true" dbType="VARCHAR" javaType="String" size="255"/>
        </column>
        <column id="pos" usedForQuery="true">
        <javadoc>
        </javadoc>
        <type canBeNull="true" dbType="VARCHAR" javaType="String" size="255"/>
        </column>
        <column id="pronunciation" isIndex="true" usedForQuery="true">
        <javadoc>
        </javadoc>
        <type canBeNull="true" dbType="VARCHAR" javaType="String" size="255"/>
        </column>
        <column id="translation" isIndex="true" usedForQuery="true">
        <javadoc>
        </javadoc>
        <type canBeNull="true" dbType="LONGVARCHAR" javaType="String"/>
        </column>
        <column id="key1" usedForQuery="true">
        <javadoc>
        </javadoc>
        <type canBeNull="true" dbType="VARCHAR" javaType="String" size="255"/>
        </column>
        <column id="key2" usedForQuery="true">
        <javadoc>
         </javadoc>
        <type canBeNull="true" dbType="VARCHAR" javaType="String" size="255"/>
        </column>
        <column id="xmlCode" usedForQuery="true">
        <javadoc>
         </javadoc>
        <type dbType="LONGVARCHAR" javaType="String"/>
        </column>
        </table>*/


    protected static String createVolumeTableParamsSql = " (" +
        "headword TEXT DEFAULT '\'''\''    ," +
		"dom BYTEA NOT NULL   ," +
		"htmldom BYTEA NOT NULL   ," +
        "xmlCode TEXT DEFAULT '\'''\'' NOT NULL   ," +

        "ObjectId DECIMAL(19,0) NOT NULL PRIMARY KEY," +
        "ObjectVersion INTEGER NOT NULL)";

    protected static String createIndexTableParamsSql = " (" +
		"key VARCHAR(255) DEFAULT '\'''\''    ," +
		"lang VARCHAR(3) DEFAULT '\'''\''    ," +
		"value VARCHAR(255) DEFAULT '\'''\''    ," +
		"entryid DECIMAL(19,0) NOT NULL    ," +
        "msort VARCHAR(255) DEFAULT '\'''\''    ," +

        "ObjectId DECIMAL(19,0) NOT NULL PRIMARY KEY," +
        "ObjectVersion INTEGER NOT NULL)";


    public static void createVolumeTable(String table) throws  PapillonBusinessException {
            executeSql(createTableSql + table + createVolumeTableParamsSql);
        }

    public static void createIndexTable(String indexTable) throws  PapillonBusinessException {
            executeSql(createTableSql + indexTable + createIndexTableParamsSql);
        }

    public static void createIndexForTable(String table, String name, String field1, String field2, String field3) throws  PapillonBusinessException {
            String query = createIndexSql + table + "_" + name  + "_idx" + " ON " + table + " ( " + field1 + "," + field2 + "," + field3 + " )";
            executeSql(query);
        }

    public static void createIndexForTable(String table, String name) throws PapillonBusinessException {
		String query = createIndexSql + table + "_" + name  + "_idx" + " ON " + table + " ( " + name + " )";
		executeSql(query);
	}

    public static void createSortIndexForVolumeTable(String table, String lang) throws PapillonBusinessException {
		String query = createIndexSql + table + "_msort_idx" + " ON " + table + " (multilingual_sort( '" + lang + "',headword ))";
		executeSql(query);
	}

    public static void dropIndexForTable(String table, String name) throws  PapillonBusinessException {
            String query = dropIndexSql + table + "_" + name + "_idx";
            executeSql(query);
        }

    public static void truncateIndexForTable(String table, String name) throws  PapillonBusinessException {
        String query = truncateTableSql + table + "_" + name + "_idx";
        executeSql(query);
    }

    public static void truncateTable(String table) throws  PapillonBusinessException {
        try {
            executeSql(truncateTableSql + table);
            //((DBTransaction) CurrentDBTransaction.get()).commit();
            //fr.imag.clips.papillon.business.PapillonLogger.writeDebugMsg("Table: " + table + " truncated");

             } catch (Exception e) {
                 throw new PapillonBusinessException("ManageDatabase.truncateTable: " + truncateTableSql + table);
             }
        }

    public static void dropTable(String table) throws  PapillonBusinessException {
        executeSql(dropTableSql + table);
        //fr.imag.clips.papillon.business.PapillonLogger.writeDebugMsg("Table: " + table + " dropped");

    }

    public static String multilingual_sort(String lang, String value) throws  PapillonBusinessException {
    //FIXME: should be called getSortKey
    String result = lang + value;

    java.util.regex.Matcher quoteMatcher = quotePattern.matcher(value);
    String newValue = quoteMatcher.replaceAll("''");

    String sql = "SELECT multilingual_sort('"+ lang + "','" + newValue + "')";
    java.util.Vector myResultVector = executeSqlQuery(sql,"multilingual_sort");

    if (myResultVector != null && myResultVector.size()>0) {
      result = (String) myResultVector.elementAt(0);
    }
    if (result.length()>255) {
      result = result.substring(0,254);
    }
    return result;
    }

	public static void createTempIndexTableFromIndexTable(String extable, String temptable, String criteria) throws PapillonBusinessException {
		String request = "SELECT DISTINCT i.key, i.lang, i.value, i.entryid, i.msort, i.objectid, i.objectversion "
				+ "INTO TABLE " + temptable + " FROM " + extable + " AS i "
                + "JOIN " + extable + " AS c ON c.entryid=i.entryid "
                + "WHERE " + criteria + ";";
		executeSql(request);
	}

	public static void renameTable(String oldname, String newname) throws PapillonBusinessException {
		String request = "ALTER TABLE " + oldname + " RENAME TO " + newname + ";";
		executeSql(request);
	}

	public static int countEntriesInIndexTable(String indexTable) {
		int result = -1;
		String request = "SELECT count(distinct entryid) FROM " + indexTable + ";";
		java.util.Vector resVector = executeSqlQuery(request, "count");
		if (resVector != null && resVector.size()>0) {
			String res = (String) resVector.elementAt(0);
			if (res !=null && !res.equals("")) {
				result = Integer.parseInt(res);
			}
		}
		return result;
	}


    private static void executeSql (String sql) throws PapillonBusinessException {
		DBTransaction transaction = CurrentDBTransaction.get();
        ManageDatabase req = new ManageDatabase(transaction, sql);

        //Flush the current transaction (?)
        // Is this really usefull ?
        if ((transaction!=null) &&
            (transaction instanceof com.lutris.appserver.server.sql.CachedDBTransaction)) {

            if(((com.lutris.appserver.server.sql.CachedDBTransaction)transaction).getAutoWrite()) try {
                transaction.write();
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                throw new PapillonBusinessException("Couldn't write transaction: "+sqle);
            }
        }

        // Create the DB Query Object
        DBQuery	dbQuery = null;

        try {
            if (transaction == null) {
                dbQuery	= DODS.getDatabaseManager().createQuery();
            } else {
                dbQuery	= transaction.createQuery();
            }

            dbQuery.query( req	);	  // invokes executeQuery

        } catch ( DatabaseManagerException e ) {
            String err = "ERROR SpecialDatabaseRequest: Could not create a DBQuery.  ";
            throw new PapillonBusinessException( err, e );
        } catch ( SQLException e ) {
            String err = "ERROR SpecialDatabaseRequest: Exception while running the query: " + sql;
            throw new PapillonBusinessException( err, e );
        } finally {
            if ( null != dbQuery ) {
                dbQuery.release();
            }
        }
    }


    public ResultSet executeQuery(DBConnection conn) throws SQLException {
        conn.execute(currentSQL);
        return null;
    }

    public static java.util.Vector getTableNames ()
        throws PapillonBusinessException {
			String sql = "SELECT tablename FROM pg_tables where tableowner='papillon'";

			java.util.Vector TableNames = new java.util.Vector();
            DBConnection myDbConnection = null;
            try {
                myDbConnection = Enhydra.getDatabaseManager().allocateConnection();
				String databaseUser = Enhydra.getApplication().getConfig().getString(DatabaseUserString);

				com.lutris.dods.builder.generator.query.QueryBuilder myQueryBuilder = new com.lutris.dods.builder.generator.query.QueryBuilder("pg_tables");
				com.lutris.dods.builder.generator.query.RDBTable pg_tables = new com.lutris.dods.builder.generator.query.RDBTable("pg_tables");
				com.lutris.dods.builder.generator.query.RDBColumn tableOwnerColumn = new com.lutris.dods.builder.generator.query.RDBColumn(pg_tables,"tableowner");
				myQueryBuilder.addWhere(tableOwnerColumn, databaseUser);
				java.sql.ResultSet myResultSet = myQueryBuilder.executeQuery(myDbConnection);
               while (myResultSet.next()) {
				   TableNames.addElement(myResultSet.getString("tablename"));
			   }
            }  catch(SQLException se) {
                //very important to throw out bad connections
				System.out.println("SQL exception: ");
				se.printStackTrace();
                if(myDbConnection.handleException(se)) myDbConnection=null;
            } catch(Exception e) {
				String err = "ERROR DatabaseConnexion: Exception while running the query: " + sql;
				System.out.println(err);
				e.printStackTrace();
				//throw new PapillonBusinessException( err, e );
            } finally {
                if(myDbConnection!=null) {
					try {
						myDbConnection.reset();
						myDbConnection.release();
					}
					catch ( SQLException e ) {
						String err = "ERROR DatabaseConnexion2: Exception while running the query: " + sql;
						System.out.println(err);
						e.printStackTrace();
//						throw new PapillonBusinessException( err, e );
					}
                }
            }
			return TableNames;
        }

	    /*
		I keep this function because the other one (executeSql) use a transaction and thus, I cannot retrieve the results because
		 the resultSet is closed.
		*/
	    protected static java.util.Vector executeSqlQuery (String sql, String columnName) {
	            DBConnection myDbConnection = null;
	            java.util.Vector resVector = new java.util.Vector();
	            try {
	                myDbConnection = Enhydra.getDatabaseManager().allocateConnection();

	                ResultSet myResultSet = myDbConnection.executeQuery(sql);

	                if(myResultSet != null) {
						while (myResultSet.next() && columnName != null) {
							resVector.addElement(myResultSet.getString(columnName));
						}
	                    myResultSet.close();
	                }
	            }  catch(SQLException se) {
	                se.printStackTrace();
	                //very important to throw out bad connections

	                if(myDbConnection.handleException(se)) myDbConnection=null;
	            } catch(Exception e) {
	                e.printStackTrace();
	            } finally {
	                if(myDbConnection!=null) {
						try {
							myDbConnection.reset();
							myDbConnection.release();
						}  catch(SQLException se) {
							se.printStackTrace();
							//very important to throw out bad connections
						if(myDbConnection.handleException(se)) myDbConnection=null;
						}
	                }
	            }
				return resVector;
	        }


/*
    protected static void simpleExecuteSql (String sql)
        throws PapillonBusinessException {
            DBConnection myDbConnection = null;
            try {
                myDbConnection = Enhydra.getDatabaseManager().allocateConnection();

                myDbConnection.execute(sql);

            }  catch(SQLException se) {
                //very important to throw out bad connections

                if(myDbConnection.handleException(se)) myDbConnection=null;
            } catch(Exception e) {
				String err = "ERROR DatabaseConnexion: Exception while running the query: " + sql;
				throw new PapillonBusinessException( err, e );
            } finally {
                if(myDbConnection!=null) {
					try {
						myDbConnection.reset();
						myDbConnection.release();
					}
					catch ( SQLException e ) {
						String err = "ERROR DatabaseConnexion: Exception while running the query: " + sql;
						throw new PapillonBusinessException( err, e );
					}
                }
            }
        } */
//
//    protected static void executeSqlQuery (String sql)
//        throws java.sql.SQLException {
//            DBConnection myDbConnection = null;
//            ResultSet myResultSet = null;
//            try {
//                myDbConnection = Enhydra.getDatabaseManager().allocateConnection();
//
//                myResultSet = myDbConnection.executeQuery(sql);
//
//                if(myResultSet != null) {
//                    myResultSet.close();
//                }
//            }  catch(SQLException se) {
//                se.printStackTrace();
//                //very important to throw out bad connections
//
//                if(myDbConnection.handleException(se)) myDbConnection=null;
//            } catch(Exception e) {
//                e.printStackTrace();
//            } finally {
//                if(myDbConnection!=null) {
//                    myDbConnection.reset();
//                    myDbConnection.release();
//                }
//            }
//        }
}
