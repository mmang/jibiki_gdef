/*
 * papillon 
 *
 * Enhydra super-servlet
 * 
 * © Mathieu Mangeot & Gilles Sérasset - GETA CLIPS IMAG
 * Projet Papillon
 *-----------------------------------------------
 * $Id: EditAuthentic.java 549 2006-09-10 09:18:25Z mangeot $
 *-----------------------------------------------
 * $Log$
 * Revision 1.1.2.3  2006/09/10 09:18:25  mangeot
 * Resoures modified and added for playing with authentic xml plugin
 *
 * Revision 1.1.2.2  2006/09/08 14:54:48  mangeot
 * *** empty log message ***
 *
 * Revision 1.1.2.1  2006/09/08 14:43:55  mangeot
 * Test d'edition d'entrees XML avec le plugin authentic
 *
 * Revision 1.1  2006/07/15 08:55:14  mangeot
 * The BrowseVolumePage opens an HTML form that is used to lookup a volume in alphabetical order.
 * The BrowseVolume is the server side of the AJAX script for retrieving the entries in alphabetical order
 *
 * Revision 1.4  2005/05/24 12:51:22  serasset
 * Updated many aspect of the Papillon project to handle lexalp project.
 * 1. Layout is now parametrable in the application configuration file.
 * 2. Notion of QueryResult has been defined to handle mono/bi and multi lingual dictionary requests
 * 3. Result presentation may be done by way of standard xsl or with any class implementing the appropriate interface.
 * 4. Enhanced dictionary edition management. The template interfaces has to be revised to be compatible.
 * 5. It is now possible to give a name to the cookie key in the app conf file
 * 6. Several bug fixes.
 *
 * Revision 1.3  2005/04/11 08:01:02  fbrunet
 * Passage en xhtml des ressources Papillon.
 *
 * Revision 1.2  2005/01/15 12:51:24  mangeot
 * Deleting old cvs comments + bug fixes with xhtml and enhydra5.1
 *
 * Revision 1.1.1.1  2004/12/06 16:38:42  serasset
 * Papillon for enhydra 5.1. This version compiles and starts with enhydra 5.1.
 * There are still bugs in the code.
 *
 *-----------------------------------------------
 * Papillon Contacts page.
 */

package fr.imag.clips.papillon.presentation;

// Enhydra SuperServlet imports
import com.lutris.appserver.server.httpPresentation.HttpPresentation;
import com.lutris.appserver.server.httpPresentation.HttpPresentationException;
import org.enhydra.xml.xmlc.XMLObject;

// Standard imports
import org.enhydra.xml.xhtml.dom.*;

import fr.imag.clips.papillon.presentation.xhtml.orig.*;
import fr.imag.clips.papillon.business.xsl.XslSheetFactory;
import fr.imag.clips.papillon.business.xsl.XslSheet;

public class EditAuthentic extends PapillonBasePO {

	public static final String SAVE_ENTRY_PARAMETER = "POSTDATA";
    public static final String EntryHandle_PARAMETER = "EntryHandle";
    public static final String VolumeName_PARAMETER = "VOLUME";  
	
    protected boolean loggedInUserRequired() {
        return true;
    }

    protected boolean userMayUseThisPO() {
        return true;
    }

    protected  int getCurrentSection() {
        return NO_SECTION;
    }

    public org.w3c.dom.Node getContent()
	throws HttpPresentationException, java.io.IOException, Exception {
//    public Node getContent()
//    throws HttpPresentationException, IOException {
		

		EditAuthenticXHTML page =
		(EditAuthenticXHTML)MultilingualXHtmlTemplateFactory.createTemplate("EditAuthenticXHTML", this.getComms(), this.getSessionData());

		String entryId = myGetParameter(EntryHandle_PARAMETER); 
		String volumeName = myGetParameter(VolumeName_PARAMETER); 
		String saveEntry = myGetParameter(SAVE_ENTRY_PARAMETER); 
		
		// sauvegarde de l'article
		if (saveEntry !=null && !saveEntry.equals("")) {
			
		}
		else {
			//On rend le contenu correct
			XHTMLElement paramXMLDataURL = page.getElementXMLDataURL();
			String xmlDataUrl = "GetXmlDocument.po?" + GetXmlDocument.TYPE_PARAMETER + "=" + GetXmlDocument.CONTRIBUTION_TYPE
				+ "&" +  GetXmlDocument.VOLUME_PARAMETER + "=" + volumeName
				+ "&" +  GetXmlDocument.ID_PARAMETER + "=" + entryId;
			paramXMLDataURL.setAttribute("value",xmlDataUrl);
				
			XHTMLElement paramSPSDataURL = page.getElementSPSDataURL();
			XslSheet myXslSheet = XslSheetFactory.findXslSheetByName(volumeName + ".sps");
			String xslSheetHandle = "";
			if (myXslSheet != null && !myXslSheet.isEmpty()) {
				xslSheetHandle = myXslSheet.getHandle();
			}
			String spsDataUrl = "GetXmlDocument.po?" + GetXmlDocument.TYPE_PARAMETER + "=" + GetXmlDocument.STYLESHEET_TYPE
				+ "&" +  GetXmlDocument.ID_PARAMETER + "=" + xslSheetHandle;
			paramSPSDataURL.setAttribute("value",spsDataUrl);

			XHTMLElement paramSchemaDataURL = page.getElementSchemaDataURL();
			String schemaDataURL = "GetXmlDocument.po?" + GetXmlDocument.TYPE_PARAMETER + "=" + GetXmlDocument.SCHEMA_TYPE
				+ "&" +  GetXmlDocument.VOLUME_PARAMETER + "=" + volumeName;
			paramSchemaDataURL.setAttribute("value",schemaDataURL);

			XHTMLElement paramXMLDataSaveUrl = page.getElementXMLDataSaveUrl();
			String xmlDataSaveUrl = this.getUrl() + "?" + EntryHandle_PARAMETER + "=" + entryId
				+ "&" +  GetXmlDocument.VOLUME_PARAMETER + "=" + volumeName;
			paramXMLDataSaveUrl.setAttribute("value",xmlDataSaveUrl);
		}
		return page.getElementHomeContent();
    }
}
