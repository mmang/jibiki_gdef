/*
 * papillon 
 *
 * Enhydra super-servlet
 * 
 * © Mathieu Mangeot & Jibiki team - GETA CLIPS IMAG
 * Projet Papillon
 *-----------------------------------------------
 * $Id$
 *-----------------------------------------------
 *-----------------------------------------------
 * Lookup Volume page.
 */

package fr.imag.clips.papillon.presentation;

// Enhydra SuperServlet imports
import com.lutris.appserver.server.httpPresentation.HttpPresentation;
import com.lutris.appserver.server.httpPresentation.HttpPresentationException;
import org.enhydra.xml.xmlc.XMLObject;

// Standard imports
import java.io.IOException;

import fr.imag.clips.papillon.presentation.xhtml.orig.*;

public class GDEFPage extends PapillonBasePO {


    protected boolean loggedInUserRequired() {
        return false;
    }

    protected boolean userMayUseThisPO() {
        return true;
    }

    protected  int getCurrentSection() {
        return NO_SECTION;
    }

	public org.w3c.dom.Node getContent() throws HttpPresentationException, IOException {
		
		String status = myGetParameter(GDEFPageXHTML.NAME_STATUS);
		String headword = myGetParameter(GDEFPageXHTML.NAME_NEWHEADWORD);

        // Création du contenu
        GDEFPageXHTML page =
            (GDEFPageXHTML)MultilingualXHtmlTemplateFactory.createTemplate("GDEFPageXHTML", this.getComms(), this.getSessionData());		
				
		if (status != null && !status.equals("")) {
			org.enhydra.xml.xhtml.dom.XHTMLInputElement statusInput = page.getElementSTATUS();
			statusInput.setValue(status);
		}
		if (headword != null && !headword.equals("")) {	
			org.enhydra.xml.xhtml.dom.XHTMLInputElement headwordInput = page.getElementNEWHEADWORD();
			headwordInput.setValue(headword);
			headwordInput.setAttribute("style","color:black");
			setHeaderScript("function loadFunction() {makeFirstRequests();return false;}");
		}
		
		
		//On rend le contenu correct
		GDEF.HIDE_MENU_COLUMN = true;
       return (org.w3c.dom.Node) page.getElementPageContent();
    }

}
