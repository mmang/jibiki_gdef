/*
 * papillon 
 *
 * Enhydra super-servlet
 * 
 * © Mathieu Mangeot - GETA CLIPS IMAG
 * Projet Papillon
 *-----------------------------------------------
 * $Id: ChangeAuthor.java 851 2008-08-03 15:07:23Z mangeot $
 *-----------------------------------------------
 * $Log$
  *
 *-----------------------------------------------
 */

package fr.imag.clips.papillon.presentation;

// Enhydra SuperServlet imports
import com.lutris.appserver.server.httpPresentation.HttpPresentation;
import com.lutris.appserver.server.httpPresentation.HttpPresentationRequest;
import com.lutris.appserver.server.httpPresentation.HttpPresentationException;
//import org.enhydra.xml.xmlc.XMLObject;
import org.enhydra.xml.xhtml.dom.*;
import org.w3c.dom.Node;
import org.w3c.dom.Text;


//pour le dictionary
import fr.imag.clips.papillon.business.dictionary.*;

import fr.imag.clips.papillon.business.user.User;
import fr.imag.clips.papillon.business.utility.Utility;
import fr.imag.clips.papillon.business.PapillonLogger;
import fr.imag.clips.papillon.business.PapillonBusinessException;


import fr.imag.clips.papillon.presentation.xhtml.orig.*;

public class ChangeAuthor extends PapillonBasePO {

	protected static final int MaxDisplayedTableEntries = 500;
	protected static final String clause = "entryid != 0";
	protected static java.util.Vector clausesVector = new java.util.Vector();
	{
		clausesVector.add(clause);
	}

    protected boolean loggedInUserRequired() {
        return true;
    }

    protected boolean userMayUseThisPO() {
        try {
            return this.getUser().isValidator();
        } catch (fr.imag.clips.papillon.business.PapillonBusinessException ex) {
            this.getSessionData().writeUserMessage("Error getting the authorisation to use this PO.");
        }
        return false;
    }
    
    protected  int getCurrentSection() {
        return NO_SECTION;
    }
    
    public Node getContent() 
        throws HttpPresentationException,
        java.io.UnsupportedEncodingException,
        org.xml.sax.SAXException,
        PapillonBusinessException {
        
        // Création du contenu
        ChangeAuthorTmplXHTML content = (ChangeAuthorTmplXHTML)MultilingualXHtmlTemplateFactory.createTemplate("ChangeAuthorTmplXHTML", this.getComms(), this.getSessionData());

   //     HttpPresentationRequest req = this.getComms().request;
		String userMessage = null;
		// decoding the CGI arguments
			String changeAuthor = myGetParameter(ChangeAuthorTmplXHTML.NAME_ChangeAuthor);

			//author
			String author = myGetParameter(ChangeAuthorTmplXHTML.NAME_AUTHOR);
			if (changeAuthor !=null && !changeAuthor.equals("")) {
				this.setPreference(ChangeAuthorTmplXHTML.NAME_AUTHOR,author);
			}
			else {
				author = this.getPreference(ChangeAuthorTmplXHTML.NAME_AUTHOR);
			}
			
			
			String volumeName = "";
			String volumeTable = "";
			 String userTempIndexTable = this.getSessionData().getTempIndexTable();
			if (userTempIndexTable != null && !userTempIndexTable.equals("")) {
				String[] Volumes = userTempIndexTable.split(EntrySelect.VOLUMES_SEP);
				if (Volumes !=null && Volumes.length==2) {
					volumeName = Volumes[0];
					volumeTable = Volumes[1];
				}
			}
	
			if (changeAuthor != null && !changeAuthor.equals("")) {
				if ((volumeTable != null && !volumeTable.equals(""))
					&& (author != null && !author.equals(""))) {
					VolumeEntriesFactory.changeAuthor(volumeName, volumeTable, this.getUser(), author, null, clausesVector);
					userMessage = "New author " + author + " for selected contributions";			
				}
			}

			if (volumeTable != null && !volumeTable.equals("")) {
				int count = addContributionsCount(content,volumeTable);
				if (count <= MaxDisplayedTableEntries) {
					addEntries(content, volumeName, volumeTable);
				}
			}
			addConsultForm(content, volumeName, volumeTable, author);
            if (null != userMessage && !userMessage.equals("")){
                this.getSessionData().writeUserMessage(userMessage);
                PapillonLogger.writeDebugMsg(userMessage);
            }

        //On rend le contenu correct
        return content.getElementFormulaire();
    }
	
	protected void addConsultForm(ChangeAuthorTmplXHTML content, String volumeName, String volumeTable, String author)  
			throws fr.imag.clips.papillon.business.PapillonBusinessException {
                    
        // Adding the user name
		User user = getUser();

		if (null != user && !user.isEmpty()) {
			content.setTextUserName(user.getName());
		}
                    
		if (volumeTable != null && !volumeTable.equals("")) {
			String label = 	volumeName + " - " + user.getLogin();
			content.setTextVolumeName(label);
		}
		else {
			XHTMLDivElement warningMessage = content.getElementWarningMessage();
			warningMessage.setAttribute("style","");
		}
               
		// author
		XHTMLInputElement authorInput = content.getElementAUTHOR();
		authorInput.setValue(author);

		removeTemplateRows(content);
    }
	
    protected void addEntries(ChangeAuthorTmplXHTML content, String volumeName, String volumeTable)
        throws PapillonBusinessException,java.io.UnsupportedEncodingException {
            // FIXME: fix the limit parameter
			
			java.util.Vector entriesVector = VolumeEntriesFactory.getVolumeNameEntriesVector(volumeName, volumeTable, null, clausesVector, null,"", 0,0);
			if (entriesVector !=null) {
				PapillonLogger.writeDebugMsg("ChangeAuthor.addEntries: entriesVector: " + entriesVector.size());
			}
			else {
				PapillonLogger.writeDebugMsg("ChangeAuthor.addEntries entriesVector null");
			}
			addEntryTable(content, entriesVector);
		}
	
	protected void addEntryTable (ChangeAuthorTmplXHTML content, java.util.Vector ContribVector)
        throws PapillonBusinessException,
        java.io.UnsupportedEncodingException {
			
            // On récupère les éléments du layout			
            XHTMLTableRowElement entryListRow = content.getElementEntryListRow();
						
			// Headword
			content.getElementHeadwordList().removeAttribute("id");
			
			// EntryId
			content.getElementEntryIdList().removeAttribute("id");
												
			// Author
			content.getElementAuthorList().removeAttribute("id");

			// Creation date
			content.getElementCreationDateList().removeAttribute("id");
															
			// Status
			content.getElementStatus().removeAttribute("id");
			
			// Reviewer
			content.getElementReviewerList().removeAttribute("id");
			
			// Creation date
			content.getElementReviewDateList().removeAttribute("id");

            // On récupère le noeud contenant la table...
            Node entryTable = entryListRow.getParentNode();
            if (null != ContribVector && ContribVector.size()>0) {
				content.setTextContributionsCount("" + ContribVector.size());
                for(int i = 0; i < ContribVector.size(); i++) {
					VolumeEntry myContrib = (VolumeEntry) ContribVector.elementAt(i);
					
					if (myContrib!=null && !myContrib.isEmpty()) {						
						// headword
						content.setTextHeadwordList(myContrib.getCompleteHeadword());

						// entry id
						content.setTextEntryIdList(myContrib.getEntryId());
						
						// author
						content.setTextAuthorList(myContrib.getAuthor());

						// creation date
						content.setTextCreationDateList(Utility.PapillonShortDateFormat.format(myContrib.getCreationDate()));
												
						// reviewer
						content.setTextReviewerList(myContrib.getReviewer());
						
						// creation date
						if (myContrib.getReviewDate()!=null) {
							content.setTextReviewDateList(Utility.PapillonShortDateFormat.format(myContrib.getReviewDate()));
						}
						else {
							content.setTextReviewDateList("");
						}
						// Status
						content.setTextStatus(myContrib.getStatus());
						                        
                        XHTMLElement clone = (XHTMLElement)entryListRow.cloneNode(true);
                        //      we have to take off the id attribute because we did not take it off the original
                        clone.removeAttribute("id");
                        entryTable.appendChild(clone);
                    }
					else {
						PapillonLogger.writeDebugMsg("contrib empty ");
					}
				}
			}
        }
	
	protected void removeTemplateRows(ChangeAuthorTmplXHTML content) {
        // EntryListRow
        org.w3c.dom.Element entryListRow = content.getElementEntryListRow();
        org.w3c.dom.Node entryListRowParent = entryListRow.getParentNode();
        entryListRowParent.removeChild(entryListRow);
	}    
	
	protected int addContributionsCount(ChangeAuthorTmplXHTML content, String table)
        throws PapillonBusinessException {
		int count = IndexFactory.countEntriesInIndexTable(table);
		content.setTextContributionsCount("" + count);
		return count;
	}

}
