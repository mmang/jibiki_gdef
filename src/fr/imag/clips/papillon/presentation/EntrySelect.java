/*
 * papillon 
 *
 * Enhydra super-servlet
 * 
 * © Mathieu Mangeot - GETA CLIPS IMAG
 * Projet Papillon
 *-----------------------------------------------
 * $Id$
 *-----------------------------------------------
 * $Log$
  *
 *-----------------------------------------------
 */

package fr.imag.clips.papillon.presentation;

// Enhydra SuperServlet imports
import com.lutris.appserver.server.httpPresentation.HttpPresentation;
import com.lutris.appserver.server.httpPresentation.HttpPresentationRequest;
import com.lutris.appserver.server.httpPresentation.HttpPresentationException;
//import org.enhydra.xml.xmlc.XMLObject;
import org.enhydra.xml.xhtml.dom.*;

//pour le dictionary
import fr.imag.clips.papillon.business.dictionary.*;

import fr.imag.clips.papillon.business.user.User;
import fr.imag.clips.papillon.business.utility.Utility;
import fr.imag.clips.papillon.business.PapillonLogger;
import fr.imag.clips.papillon.business.PapillonBusinessException;


import fr.imag.clips.papillon.presentation.xhtml.orig.*;


public class EntrySelect extends PapillonBasePO {

	protected final static String VOLUMES_SEP = "#";
	protected final static java.util.regex.Pattern ixe = java.util.regex.Pattern.compile("x");
	protected static final int MaxDisplayedTableEntries = 500;
	protected static final String clause = "entryid != 0";
	protected static java.util.Vector clausesVector = new java.util.Vector();
	{
		clausesVector.add(clause);
	}

    protected boolean loggedInUserRequired() {
        return true;
    }

    protected boolean userMayUseThisPO() {
        try {
            return this.getUser().isValidator();
        } catch (fr.imag.clips.papillon.business.PapillonBusinessException ex) {
            this.getSessionData().writeUserMessage("Error getting the authorisation to use this PO.");
        }
        return false;
    }
    
    protected  int getCurrentSection() {
        return NO_SECTION;
    }
    
    public org.w3c.dom.Node getContent() 
        throws HttpPresentationException,
        java.io.UnsupportedEncodingException,
        org.xml.sax.SAXException,
        PapillonBusinessException {
        
        // Création du contenu
        EntrySelectTmplXHTML content = (EntrySelectTmplXHTML)MultilingualXHtmlTemplateFactory.createTemplate("EntrySelectTmplXHTML", this.getComms(), this.getSessionData());

   //     HttpPresentationRequest req = this.getComms().request;
        
		// decoding the CGI arguments
		String volumes = myGetParameter(EntrySelectTmplXHTML.NAME_VOLUME);
						
		if (volumes!=null &&!volumes.equals("")) {
			this.setPreference(EntrySelectTmplXHTML.NAME_VOLUME,volumes);
		}
		else {
			volumes = this.getPreference(EntrySelectTmplXHTML.NAME_VOLUME);
		}
		String volume = "";
		String volumeTable = "";
		if (volumes!=null && !volumes.equals("")) {
			String[] Volumes = volumes.split(VOLUMES_SEP);
			if (Volumes !=null && Volumes.length==2) {
				volume = Volumes[0];
				volumeTable = Volumes[1];
			}
		}

		String submitSelect = myGetParameter(EntrySelectTmplXHTML.NAME_SELECT);
		String submitIndex = myGetParameter(EntrySelectTmplXHTML.NAME_INDEX);
		String submitCount = myGetParameter(EntrySelectTmplXHTML.NAME_COUNT);

		if (submitSelect!= null && !submitSelect.equals("")) {
		String Criteria1 = myGetParameter(EntrySelectTmplXHTML.NAME_Criterion1);
		Volume myVolume = VolumesFactory.findVolumeByName(volume);
		String Criteria1text = myGetParameter(EntrySelectTmplXHTML.NAME_Criterion1text);

		String strategy1 = myGetParameter(EntrySelectTmplXHTML.NAME_Strategy1);
		java.util.regex.Matcher matchixe1 = ixe.matcher(strategy1);
		if (matchixe1.find()) {
			strategy1 = "c.value" + matchixe1.replaceFirst(Criteria1text);
		}
		else if (strategy1.equals("<") || strategy1.equals(">") && Volume.isLangCDMElement(Criteria1)) {
			String lang=null;
			if (Volume.isSourceLangCDMElement(Criteria1)) {
				lang = myVolume.getSourceLanguage();
			}
			else {
				String[] targets = myVolume.getTargetLanguagesArray();
				if (targets != null && targets.length>0) {
					lang = targets[0];
				}
			}
			if (lang !=null && !lang.equals("")) {
				strategy1 = "c.msort" + strategy1 + "multilingual_sort('" + lang + "','" + Criteria1text + "')";
			}
			else {
				strategy1 = "c.value" + strategy1 + "'" + Criteria1text + "'";
			}
		}
		else {
			strategy1 = "c.value" + strategy1 + "'" + Criteria1text + "'";
		}
		Criteria1 = "(c.key='" + Criteria1 + "' AND " + strategy1 + ")";
		
		String Coordination1 = myGetParameter(EntrySelectTmplXHTML.NAME_Coordination1);
		if (!Coordination1.equals("")) {
			Criteria1 += " " + Coordination1 + " ";
			
			String Criteria2 = myGetParameter(EntrySelectTmplXHTML.NAME_Criterion2);
		
			String Criteria2text = myGetParameter(EntrySelectTmplXHTML.NAME_Criterion2text);

			String strategy2 = myGetParameter(EntrySelectTmplXHTML.NAME_Strategy2);
			java.util.regex.Matcher matchixe2 = ixe.matcher(strategy2);			
			if (matchixe2.find()) {
				strategy2 = "c.value" + matchixe2.replaceFirst(Criteria2text);
			}
			else if (strategy2.equals("<") || strategy2.equals(">") && Volume.isLangCDMElement(Criteria2)) {
				String lang2=null;
				if (Volume.isSourceLangCDMElement(Criteria2)) {
					lang2 = myVolume.getSourceLanguage();
				}
				else {
					String[] targets = myVolume.getTargetLanguagesArray();
					if (targets != null && targets.length>0) {
						lang2 = targets[0];
					}
				}
				if (lang2 !=null && !lang2.equals("")) {
					strategy2 = "c.msort" + strategy2 + "multilingual_sort('" + lang2 + "','" + Criteria2text + "')";
				}
				else {
					strategy2 = "c.value" + strategy2 + "'" + Criteria2text + "'";
				}
			}
			else {
				strategy2 = "c.value" + strategy2 + "'" + Criteria2text + "'";
			}
			Criteria1 += "(c.key='" + Criteria2 + "' AND "+ strategy2 + ")";
		}
		String userTable = myVolume.getIndexDbname() + this.getUser().getLogin();
		PapillonLogger.writeDebugMsg("volume: " + volume);
		PapillonLogger.writeDebugMsg("volumeTable: " + volumeTable);
		PapillonLogger.writeDebugMsg("userTable: " + userTable);
		PapillonLogger.writeDebugMsg("criteria: " + Criteria1);
		IndexFactory.createUserIndexTableFromIndexTable(volumeTable,userTable, Criteria1);
		this.getSessionData().setTempIndexTable(volume + VOLUMES_SEP + userTable);
		int count = addContributionsCount(content,userTable);
		if (count <= MaxDisplayedTableEntries) {
			addEntries(content, volume, userTable);
		}
	}
		if (submitCount!= null && !submitCount.equals("")) {
			int count = addContributionsCount(content,volumeTable);
			if (count <= MaxDisplayedTableEntries) {
				addEntries(content, volume, volumeTable);
			}
		}
		if (submitIndex!= null && !submitIndex.equals("")) {
			IndexFactory.createIndexForTable(volumeTable);
		}

        addConsultForm(content,volumes);
		removeTemplateRows(content);

        //On rend le contenu correct
        return content.getElementFormulaire();
    }
	
	protected void addConsultForm(EntrySelectTmplXHTML content, String volume) 
		throws fr.imag.clips.papillon.business.PapillonBusinessException {
                    
        // Adding the user name
		User user = getUser();

		if (null != user && !user.isEmpty()) {
			content.setTextUserName(user.getName());
		}
                    
           // Adding the appropriate source languages to the source list
        XHTMLOptionElement volumeOptionTemplate = content.getElementVolumeOptionTemplate();
        org.w3c.dom.Node volumeSelect = volumeOptionTemplate.getParentNode();
        volumeOptionTemplate.removeAttribute("id");
        // We assume that the option element has only one text child 
        // (it should be this way if the HTML is valid...)
        org.w3c.dom.Text volumeTextTemplate = (org.w3c.dom.Text)volumeOptionTemplate.getFirstChild(); 
                
                
        Volume[] AllVolumes = VolumesFactory.getVolumesArray();
        
 		String userTempIndexTable = this.getSessionData().getTempIndexTable();
		if (userTempIndexTable != null && !userTempIndexTable.equals("")) {
			String[] Volumes = userTempIndexTable.split(VOLUMES_SEP);
			String volumeName = "";
			volume = "";
			if (Volumes !=null && Volumes.length==2) {
				volumeName = Volumes[0];
			}
			String label = 	volumeName + " - " + user.getLogin();
			volumeOptionTemplate.setValue(userTempIndexTable);
            volumeOptionTemplate.setLabel(label);
            // Je dois ici mettre un text dans l'OPTION, car les browser PC ne sont pas conformes aux 
            // specs W3C.
            volumeOptionTemplate.setSelected(true);
            volumeTextTemplate.setData(label);
            volumeSelect.appendChild(volumeOptionTemplate.cloneNode(true));
		}
        for (int i = 0; i < AllVolumes.length; i++) {
			Volume myVolume = AllVolumes[i];
			String schema = myVolume.getXmlSchema();
			if (schema != null && !schema.equals("")) {
			String idxname = myVolume.getName() + VOLUMES_SEP + myVolume.getIndexDbname();
            volumeOptionTemplate.setValue(idxname);
            volumeOptionTemplate.setLabel(myVolume.getName());
            // Je dois ici mettre un text dans l'OPTION, car les browser PC ne sont pas conformes aux 
            // specs W3C.
            volumeOptionTemplate.setSelected(idxname.equals(volume));
            volumeTextTemplate.setData(myVolume.getName());
            volumeSelect.appendChild(volumeOptionTemplate.cloneNode(true));
			}
		}
        volumeSelect.removeChild(volumeOptionTemplate);
	}

   protected void addEntries(EntrySelectTmplXHTML content, String volumeName, String volumeTable)
        throws PapillonBusinessException,java.io.UnsupportedEncodingException {
            // FIXME: fix the limit parameter
			
			java.util.Vector entriesVector = VolumeEntriesFactory.getVolumeNameEntriesVector(volumeName, volumeTable, null, clausesVector, null,"", 0,0);
			if (entriesVector !=null) {
				PapillonLogger.writeDebugMsg("ChangeAuthor.addEntries: entriesVector: " + entriesVector.size());
			}
			else {
				PapillonLogger.writeDebugMsg("ChangeAuthor.addEntries entriesVector null");
			}
			addEntryTable(content, entriesVector);
		}
	
	protected void addEntryTable (EntrySelectTmplXHTML content, java.util.Vector ContribVector)
        throws PapillonBusinessException,
        java.io.UnsupportedEncodingException {
			
            // On récupère les éléments du layout			
            XHTMLTableRowElement entryListRow = content.getElementEntryListRow();
						
			// Headword
			content.getElementHeadwordList().removeAttribute("id");
			
			// EntryId
			content.getElementEntryIdList().removeAttribute("id");
												
			// Author
			content.getElementAuthorList().removeAttribute("id");

			// Creation date
			content.getElementCreationDateList().removeAttribute("id");
															
			// Status
			content.getElementStatus().removeAttribute("id");
			
			// Reviewer
			content.getElementReviewerList().removeAttribute("id");
			
			// Creation date
			content.getElementReviewDateList().removeAttribute("id");

            // On récupère le noeud contenant la table...
            org.w3c.dom.Node entryTable = entryListRow.getParentNode();
            if (null != ContribVector && ContribVector.size()>0) {
				content.setTextContributionsCount("" + ContribVector.size());
                for(int i = 0; i < ContribVector.size(); i++) {
					VolumeEntry myContrib = (VolumeEntry) ContribVector.elementAt(i);
					
					if (myContrib!=null && !myContrib.isEmpty()) {						
						// headword
						content.setTextHeadwordList(myContrib.getCompleteHeadword());

						// entry id
						content.setTextEntryIdList(myContrib.getEntryId());
						
						// author
						content.setTextAuthorList(myContrib.getAuthor());

						// creation date
						content.setTextCreationDateList(Utility.PapillonShortDateFormat.format(myContrib.getCreationDate()));
												
						// reviewer
						content.setTextReviewerList(myContrib.getReviewer());
						
						// creation date
						if (myContrib.getReviewDate()!=null) {
							content.setTextReviewDateList(Utility.PapillonShortDateFormat.format(myContrib.getReviewDate()));
						}
						else {
							content.setTextReviewDateList("");
						}
						// Status
						content.setTextStatus(myContrib.getStatus());
						                        
                        XHTMLElement clone = (XHTMLElement)entryListRow.cloneNode(true);
                        //      we have to take off the id attribute because we did not take it off the original
                        clone.removeAttribute("id");
                        entryTable.appendChild(clone);
                    }
					else {
						PapillonLogger.writeDebugMsg("contrib empty ");
					}
				}
			}
        }
	
	protected void removeTemplateRows(EntrySelectTmplXHTML content) {
        // EntryListRow
        org.w3c.dom.Element entryListRow = content.getElementEntryListRow();
        org.w3c.dom.Node entryListRowParent = entryListRow.getParentNode();
        entryListRowParent.removeChild(entryListRow);
	}    

	
	protected int addContributionsCount(EntrySelectTmplXHTML content, String table)
        throws PapillonBusinessException {
		int count = IndexFactory.countEntriesInIndexTable(table);
		content.setTextContributionsCount("" + count);
		return count;
	}
}
