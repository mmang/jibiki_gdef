/*
 *  papillon
 *
 *  Enhydra super-servlet presentation object
 *
 *  © Mathieu Mangeot & Gilles Sérasset - GETA CLIPS IMAG
 *  Projet Papillon
 *
 *  -----------------------------------------------
 *  $Id: Home.java 1064 2009-08-19 20:02:12Z mangeot $
 *  -----------------------------------------------
 *  $Log$
  *  -----------------------------------------------
 *
 */
package fr.imag.clips.papillon.presentation;

// Enhydra SuperServlet imports
import com.lutris.appserver.server.httpPresentation.HttpPresentation;
//import com.lutris.appserver.server.httpPresentation.HttpPresentationComms;
import com.lutris.appserver.server.httpPresentation.HttpPresentationRequest;
import com.lutris.appserver.server.httpPresentation.HttpPresentationException;
import com.lutris.appserver.server.httpPresentation.ClientPageRedirectException;

//import org.enhydra.xml.xmlc.XMLObject;
import org.enhydra.xml.xhtml.dom.*;

// Standard imports
import java.io.IOException;
import java.io.*;
import java.util.*;
import java.util.Date;
import java.text.DateFormat;

// Imported DOM classes
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fr.imag.clips.papillon.business.dictionary.Volume;
import fr.imag.clips.papillon.business.dictionary.VolumeEntry;
import fr.imag.clips.papillon.business.dictionary.VolumesFactory;
import fr.imag.clips.papillon.business.utility.Utility;

import fr.imag.clips.papillon.business.PapillonLogger;
import fr.imag.clips.papillon.business.PapillonBusinessException;

import fr.imag.clips.papillon.presentation.xhtml.orig.*;

/**
 *  Description of the Class
 *
 * @author     serasset
 * @created    December 8, 2004
 */
public class Home extends PapillonBasePO {
		
	protected static int GDEF_estValidatedEntriesCount = 0;
	
	protected static int GDEF_estReviewedEntriesCount = 0;
	
	protected static int GDEF_estFinishedEntriesCount = 0;
	
	protected static java.util.Calendar myCalendar = new java.util.GregorianCalendar();
	
	protected static int DAY_OF_MONTH = 0;
	
	public static boolean HIDE_MENU_COLUMN = false;
	
	public final static String ALL_TARGETS = "*ALL*";

		
    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    protected boolean loggedInUserRequired() {
        return false;
    }
	
	
    /**
     *  Description of the Method
     *
     * @return    Description of the Return Value
     */
    protected boolean userMayUseThisPO() {
        return true;
    }
	
	
    /**
     *  Gets the currentSection attribute of the Home object
     *
     * @return    The currentSection value
     */
    protected int getCurrentSection() {
        return NO_SECTION;
    }
	
	
    /**
     *  Gets the content attribute of the Home object
     *
     * @return                                                     The content
     *      value
     * @exception  HttpPresentationException                       Description
     *      of the Exception
     * @exception  IOException                                     Description
     *      of the Exception
     * @exception  TransformerConfigurationException               Description
     *      of the Exception
     * @exception  org.xml.sax.SAXException                        Description
     *      of the Exception
     * @exception  javax.xml.parsers.ParserConfigurationException  Description
     *      of the Exception
     * @exception  java.io.IOException                             Description
     *      of the Exception
     * @exception  javax.xml.transform.TransformerException        Description
     *      of the Exception
     * @exception  ClassNotFoundException                          Description
     *      of the Exception
     * @exception  PapillonBusinessException                       Description
     *      of the Exception
     * @exception  UnsupportedEncodingException                    Description
     *      of the Exception
     */
    public Node getContent()
	throws HttpPresentationException,
	java.io.IOException {
		
		
        // On regarde d'abord les parametres qui nous sont demandes.
        String submit = myGetParameter(GDEFContentXHTML.NAME_LOOKUP);
		HIDE_MENU_COLUMN = true;
				
		throw new ClientPageRedirectException("GDEF.po");
		//return createGDEFContent();
    }
	
    /**
     *  Adds a feature to the HomeContent attribute of the Home object
     *
     * @exception  HttpPresentationException  Description of the Exception
     * @exception  java.io.IOException        Description of the Exception
     */
    public Node createGDEFContent()
	throws HttpPresentationException,
	java.io.IOException {
        GDEFContentXHTML homeContent = (GDEFContentXHTML) MultilingualXHtmlTemplateFactory.createTemplate("GDEFContentXHTML", this.getComms(), this.getSessionData());
        Element home = homeContent.getElementHomeContent();
        Element projectDescription = homeContent.getElementProjectDescription();
		
 		// code spécifique pour le GDEF
		Element GDEFValidatedEntryCountFra = home.getOwnerDocument().getElementById("GDEFValidatedEntryCountFra");
		if (GDEFValidatedEntryCountFra != null) {
			Volume GDEFVolume = VolumesFactory.findVolumeByName("GDEF_est");
			if (GDEFVolume!=null) {
				myCalendar = new java.util.GregorianCalendar();
				if (myCalendar.get(myCalendar.DAY_OF_MONTH) != DAY_OF_MONTH) {
					GDEF_estValidatedEntriesCount = GDEFVolume.getCount(VolumeEntry.VALIDATED_STATUS);
					GDEF_estReviewedEntriesCount = GDEFVolume.getCount(VolumeEntry.REVIEWED_STATUS);
					GDEF_estFinishedEntriesCount = GDEFVolume.getCount(VolumeEntry.FINISHED_STATUS);
					DAY_OF_MONTH = myCalendar.get(myCalendar.DAY_OF_MONTH);
				}
				int total = GDEF_estValidatedEntriesCount + GDEF_estReviewedEntriesCount + GDEF_estFinishedEntriesCount;
				Utility.setText(GDEFValidatedEntryCountFra,"" + GDEF_estValidatedEntriesCount);
			}
		}
        return (Node) home;
    }
}

