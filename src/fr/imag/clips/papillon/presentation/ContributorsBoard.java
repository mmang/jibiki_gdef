/*
 * papillon 
 *
 * Enhydra super-servlet
 * 
 * © Mathieu Mangeot & Gilles Sérasset - GETA CLIPS IMAG
 * Projet Papillon
 *-----------------------------------------------
 * $Id: ContributorsBoard.java 689 2007-08-26 16:54:14Z mangeot $
 *-----------------------------------------------
 * $Log$
 * Revision 1.8.2.15  2007/08/26 16:54:14  mangeot
 * Cosme
 *
 * Revision 1.8.2.14  2007/08/26 16:44:36  mangeot
 * Cosme
 *
 * Revision 1.8.2.13  2007/08/26 16:39:14  mangeot
 * Cosmetics
 *
 * Revision 1.8.2.12  2007/08/26 16:30:51  mangeot
 * cosmetic fix for contributors chart
 *
 * Revision 1.8.2.11  2007/08/26 16:22:11  mangeot
 * bug fix for percentage chart
 *
 * Revision 1.8.2.10  2007/08/26 16:13:33  mangeot
 * fix percentage disparition
 *
 * Revision 1.8.2.9  2007/08/25 14:17:45  mangeot
 * Ajout du graphique absolu
 *
 * Revision 1.8.2.8  2007/08/25 13:44:05  mangeot
 * percentage bug fix
 *
 * Revision 1.8.2.7  2007/08/25 13:28:39  mangeot
 * XHTML bug fix
 *
 * Revision 1.8.2.6  2007/08/25 13:02:18  mangeot
 * Elaborating bar chart for contributors board
 *
 * Revision 1.8.2.5  2007/08/25 09:19:40  mangeot
 * changement de date de fin pour le mois en cours
 *
 * Revision 1.8.2.4  2007/08/25 09:15:20  mangeot
 * Le mois commence à zéro !
 *
 * Revision 1.8.2.3  2007/08/25 08:48:15  mangeot
 * tableau resume pour le mois en cours
 * bouton séparé pour le tableau des pourcentages
 *
 * Revision 1.8.2.2  2007/08/25 08:04:38  mangeot
 * Ajout du tableau des pourcentages pour Jean Pascal
 *
 * Revision 1.8.2.1  2006/08/20 14:57:19  mangeot
 * *** empty log message ***
 *
 * Revision 1.8  2006/02/26 14:04:56  mangeot
 * Corrected a bug: the content was a static variable, thus there were problems when two users wanted to aces the same page at the same time
 *
 * Revision 1.7  2005/11/01 14:12:02  mangeot
 * *** empty log message ***
 *
 * Revision 1.6  2005/10/21 12:24:16  mangeot
 * *** empty log message ***
 *
 * Revision 1.5  2005/10/21 12:18:10  mangeot
 * *** empty log message ***
 *
 * Revision 1.4  2005/10/21 12:15:35  mangeot
 * *** empty log message ***
 *
 * Revision 1.3  2005/10/21 11:20:46  mangeot
 * Modified the contributors board to add the best contributor and best reviewer
 *
 * Revision 1.2  2005/06/15 16:48:28  mangeot
 * Merge between the ContribsInXml branch and the main trunk. It compiles but bugs remain..
 *
 * Revision 1.1.2.3  2005/05/25 21:00:36  mangeot
 * Bug fixes
 *
 * Revision 1.1.2.2  2005/05/24 11:15:48  mangeot
 * Bug fixes in sort
 *
 * Revision 1.1.2.1  2005/05/20 10:31:46  mangeot
 * Added 2 new classes 1 contributors board and one for exporting a volume
 *
 *
 *-----------------------------------------------
 * Papillon Contributors Board page.
 */

package fr.imag.clips.papillon.presentation;

// Enhydra SuperServlet imports
import com.lutris.appserver.server.httpPresentation.HttpPresentation;
import com.lutris.appserver.server.httpPresentation.HttpPresentationRequest;
import com.lutris.appserver.server.httpPresentation.HttpPresentationException;
import com.lutris.appserver.server.session.Session;

import org.enhydra.xml.xhtml.dom.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import fr.imag.clips.papillon.business.message.MessageDBLoader;
import fr.imag.clips.papillon.presentation.PapillonSessionData;

// Standard imports
import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.io.*;

// for users
import fr.imag.clips.papillon.business.user.*;
import fr.imag.clips.papillon.presentation.xhtml.orig.ContributorsBoardTmplXHTML;

import fr.imag.clips.papillon.data.*;
import fr.imag.clips.papillon.business.utility.Utility;
import fr.imag.clips.papillon.business.transformation.*;
import fr.imag.clips.papillon.business.PapillonLogger;
import fr.imag.clips.papillon.business.dictionary.VolumeEntry;


public class ContributorsBoard extends PapillonBasePO {

    protected final static String SORTBY_PARAMETER="SortBy";
	protected final static String ALL="*ALL*";
    
    protected ContributorsBoardTmplXHTML content;

    protected boolean loggedInUserRequired() {
        return true;
    }

    protected boolean userMayUseThisPO() {
        try {
            return this.getUser().isSpecialist();
        } catch (fr.imag.clips.papillon.business.PapillonBusinessException ex) {
            this.getSessionData().writeUserMessage("Error getting the authorisation to use this PO.");
        }
        return false;
    }
	
    protected  int getCurrentSection() {
        return NO_SECTION;
    }

    public Node getContent()
        throws javax.xml.parsers.ParserConfigurationException,
			HttpPresentationException,
		    IOException, org.xml.sax.SAXException,
			javax.xml.transform.TransformerException,
			fr.imag.clips.papillon.presentation.PapillonPresentationException {
        
        // Création du contenu
        content = (ContributorsBoardTmplXHTML)MultilingualXHtmlTemplateFactory.createTemplate("ContributorsBoardTmplXHTML", this.getComms(), this.getSessionData());
	  
        HttpPresentationRequest req = this.getComms().request;

		String lookup =  myGetParameter(content.NAME_LOOKUP);
		String lookupMonth =  myGetParameter(content.NAME_LOOKUP_MONTH);
		String lookupPercent =  myGetParameter(content.NAME_LOOKUP_PERCENTAGE);
		String volume =  myGetParameter(content.NAME_VOLUME);
		String fromDateString =  myGetParameter(content.NAME_FromDate);
		String toDateString =  myGetParameter(content.NAME_ToDate);
		String sortBy =  myGetParameter(SORTBY_PARAMETER);

		String userMessage = "";
		if (lookup !=null && !lookup.equals("")
			&& fromDateString !=null && !fromDateString.equals("")
			&& toDateString !=null && !toDateString.equals("")) {
			
			this.setPreference(content.NAME_VOLUME,volume);
			if (volume != null && (volume.equals("") || volume.equals(ALL))) {
				volume = null;
			}
			
			this.setPreference(content.NAME_FromDate,fromDateString);
			this.setPreference(content.NAME_ToDate,toDateString);

			java.util.Date fromDate = null;
			java.util.Date toDate = null;
			
			try {
				fromDate = Utility.PapillonShortDateFormat.parse(fromDateString);
				toDate = Utility.PapillonShortDateFormat.parse(toDateString);
			}
			catch (java.text.ParseException pe) {
				userMessage = "Error in date format, please check!";
			}
			
			addContributorsBoard(volume, fromDate, toDate, sortBy);
		}
			else if (lookupMonth !=null && !lookupMonth.equals("")) {
				java.util.Date fromDate = null;
				java.util.Date toDate = null;
				java.util.Calendar calendar = new java.util.GregorianCalendar();
				calendar.setTime(new java.util.Date());
				try {
					toDate = Utility.PapillonShortDateFormat.parse("9999/01/01");
					fromDate = Utility.PapillonShortDateFormat.parse(""+calendar.get(java.util.Calendar.YEAR)+"/"+(calendar.get(java.util.Calendar.MONTH)+1)+ "/01");
				}
				catch (java.text.ParseException pe) {
					userMessage = "Error in date format, please check!";
				}
				addContributorsBoard(volume, fromDate, toDate, sortBy);
			}
			else if (lookupPercent !=null && !lookupPercent.equals("") && 
					 volume != null && !volume.equals("")  && !volume.equals(ALL)) {
				addContributorsPercentage(volume, sortBy);
			}
			else {
			volume = this.getPreference(content.NAME_VOLUME);
			fromDateString = this.getPreference(content.NAME_FromDate);
			toDateString = this.getPreference(content.NAME_ToDate);

		}
		if (userMessage != null) {
			this.getSessionData().writeUserMessage(userMessage);
			PapillonLogger.writeDebugMsg(userMessage);
		}
		addBoardForm(volume, fromDateString, toDateString);
		
        //On rend le contenu correct
        return content.getElementFormulaire();
    }


    protected void addContributorsBoard(String volume, java.util.Date fromDate, java.util.Date toDate, String sortBy)
        throws fr.imag.clips.papillon.business.PapillonBusinessException {

            java.util.Vector UsersVector =  fr.imag.clips.papillon.business.dictionary.ContributionsFactory.getContributorsBoard(volume, fromDate, toDate, sortBy);
			
            //where we must insert the form
            XHTMLTableRowElement theRow = content.getElementTemplateRow();
            XHTMLElement theName = content.getElementName();
            XHTMLElement theLogin = content.getElementLogin();
			XHTMLElement theTableDiv = content.getElementContributorsTable();
			theTableDiv.setAttribute("class","");

            Node theRowParent = theRow.getParentNode();

            theRow.removeAttribute("id");
            theName.removeAttribute("id");
            theLogin.removeAttribute("id");
			
            //adding the volumes description
			int contributionsMax = 0;
			int revisionsMax = 0;
			int validationsMax = 0;
			int contributionsTotal = 0;
			int revisionsTotal = 0;
			int validationsTotal = 0;
			String bestContributor = "";
			String bestReviewer = "";
			String bestValidator = "";
				
			for (int i=0; i< UsersVector.size(); i++) {
				java.util.Vector myVector = (java.util.Vector) UsersVector.elementAt(i);
				User myUser = (User) myVector.elementAt(0);
				int finished = Integer.parseInt((String) myVector.elementAt(1));
				int reviewed = Integer.parseInt((String) myVector.elementAt(2));
				int validated = Integer.parseInt((String) myVector.elementAt(3));
				contributionsTotal += finished;
				revisionsTotal += reviewed;
				validationsTotal += validated;
				content.setTextName(myUser.getName());
				content.setTextLogin(myUser.getLogin());
				content.setTextFinished("" + finished);
				content.setTextReviewed("" + reviewed);
				content.setTextValidated("" + validated);
				if (contributionsMax < finished) {
					contributionsMax = finished;
					bestContributor = myUser.getName();
				}
				if (revisionsMax < reviewed) {
					revisionsMax = reviewed;
					bestReviewer = myUser.getName();
				}
				theRowParent.appendChild(theRow.cloneNode(true));
            }
            theRowParent.removeChild(theRow);
			content.setTextBestContributor(bestContributor);
			content.setTextMaxContributions("" + contributionsMax);
 			content.setTextBestReviewer(bestReviewer);
			content.setTextMaxRevisions("" + revisionsMax);
			
			content.setTextTotalFinished("" + contributionsTotal);
			content.setTextTotalReviewed("" + revisionsTotal);
			content.setTextTotalValidated("" + validationsTotal);
        }

	
	protected void addContributorsPercentage(String volume, String sortBy)
        throws fr.imag.clips.papillon.business.PapillonBusinessException {
			
            java.util.Vector UsersTable =  fr.imag.clips.papillon.business.dictionary.ContributionsFactory.getContributorsPercentage(volume, sortBy);
			
            //where we must insert the form
            XHTMLElement percentageRow = content.getElementPercentageTemplateRow();
            XHTMLElement absoluteRow = content.getElementAbsoluteTemplateRow();
            XHTMLElement percentageName = content.getElementPercentageName();
            XHTMLElement absoluteName = content.getElementAbsoluteName();
			XHTMLElement theTableDiv = content.getElementContributorsPercentageTable();
			theTableDiv.setAttribute("class","");
			
            Node percentageRowParent = percentageRow.getParentNode();
            Node absoluteRowParent = absoluteRow.getParentNode();
			
            percentageRow.removeAttribute("id");
            absoluteRow.removeAttribute("id");
            percentageName.removeAttribute("id");
            absoluteName.removeAttribute("id");
									
			for (int i=0; i< UsersTable.size(); i++) {
				java.util.Vector myVector = (java.util.Vector) UsersTable.elementAt(i);
				User myUser = (User) myVector.elementAt(0);
				java.util.Hashtable statusTable = (java.util.Hashtable) myVector.elementAt(1);
				int finished = Integer.parseInt((String) statusTable.get(VolumeEntry.FINISHED_STATUS));
				int reviewed = Integer.parseInt((String) statusTable.get(VolumeEntry.REVIEWED_STATUS));
				int validated = Integer.parseInt((String) statusTable.get(VolumeEntry.VALIDATED_STATUS));
				int totalContributions = finished + reviewed + validated;
				int finishedPercent = finished * 100 / totalContributions;
				int reviewedPercent = reviewed * 100 / totalContributions;
				int validatedPercent = validated * 100 / totalContributions;
				content.setTextPercentageName(myUser.getName());
				
				XHTMLElement theFinishedDiv = content.getElementPercentageFinishedValue();
				theFinishedDiv.setAttribute("style","width:"+(finishedPercent*5)+"px;");
				if (finishedPercent>5) {
					content.setTextPercentageFinishedLegend("" + finishedPercent + "%");
				}
				else if (finishedPercent>0) {
					content.setTextPercentageFinishedLegend(".");
				}
				else {
					content.setTextPercentageFinishedLegend("");
				}
				
				XHTMLElement theReviewedDiv = content.getElementPercentageReviewedValue();
				theReviewedDiv.setAttribute("style","width:"+(reviewedPercent*5)+"px;");
				if (reviewedPercent>5) {
					content.setTextPercentageReviewedLegend("" + reviewedPercent + "%");
				}
				else if (reviewedPercent>0) {
					content.setTextPercentageReviewedLegend(".");
				}
				else {
					content.setTextPercentageReviewedLegend("");
				}
				
				XHTMLElement theValidatedDiv = content.getElementPercentageValidatedValue();
				theValidatedDiv.setAttribute("style","width:"+(validatedPercent*5)+"px;");
				content.setTextPercentageValidatedLegend("" + validatedPercent + "%");
				if (validatedPercent>5) {
					content.setTextPercentageValidatedLegend("" + validatedPercent + "%");
				}
				else if (validatedPercent>0) {
					content.setTextPercentageValidatedLegend(".");
				}
				else {
					content.setTextPercentageValidatedLegend("");
				}
				
				percentageRowParent.appendChild(percentageRow.cloneNode(true));
            }
            percentageRowParent.removeChild(percentageRow);

			for (int i=0; i< UsersTable.size(); i++) {
				java.util.Vector myVector = (java.util.Vector) UsersTable.elementAt(i);
				User myUser = (User) myVector.elementAt(0);
				java.util.Hashtable statusTable = (java.util.Hashtable) myVector.elementAt(1);
				int finished = Integer.parseInt((String) statusTable.get(VolumeEntry.FINISHED_STATUS));
				int reviewed = Integer.parseInt((String) statusTable.get(VolumeEntry.REVIEWED_STATUS));
				int validated = Integer.parseInt((String) statusTable.get(VolumeEntry.VALIDATED_STATUS));
				int totalContributions = finished + reviewed + validated;
				content.setTextAbsoluteName(myUser.getName());
				
				XHTMLElement theFinishedDiv = content.getElementAbsoluteFinishedValue();
				theFinishedDiv.setAttribute("style","width:"+(finished/2)+"px;");
				if (finished>35) {
					content.setTextAbsoluteFinishedLegend("" + finished + "");
				}
				else if (finished>0) {
					content.setTextAbsoluteFinishedLegend(".");
				}
				else {
					content.setTextAbsoluteFinishedLegend("");
				}
				
				XHTMLElement theReviewedDiv = content.getElementAbsoluteReviewedValue();
				theReviewedDiv.setAttribute("style","width:"+(reviewed/2)+"px;");
				if (reviewed>35) {
					content.setTextAbsoluteReviewedLegend("" + reviewed + "");
				}
				else if (reviewed>0) {
					content.setTextAbsoluteReviewedLegend(".");
				}
				else {
					content.setTextAbsoluteReviewedLegend("");
				}
				
				XHTMLElement theValidatedDiv = content.getElementAbsoluteValidatedValue();
				theValidatedDiv.setAttribute("style","width:"+(validated/2)+"px;");
				if (validated>35) {
					content.setTextAbsoluteValidatedLegend("" + validated + "");
				}
				else if (validated>0) {
					content.setTextAbsoluteValidatedLegend(".");
				}
				else {
					content.setTextAbsoluteValidatedLegend("");
				}
				
				content.setTextAbsoluteTotalLegend("" + totalContributions);
				absoluteRowParent.appendChild(absoluteRow.cloneNode(true));
            }
            absoluteRowParent.removeChild(absoluteRow);			
		
		}
	
	
	protected void addBoardForm(String selectedVolume, String fromDate, String toDate)
		throws fr.imag.clips.papillon.business.PapillonBusinessException {
	
		// volume
        XHTMLOptionElement volumeOptionTemplate = content.getElementVolumeOptionTemplate();
        Node volumeSelect = volumeOptionTemplate.getParentNode();
        volumeOptionTemplate.removeAttribute("id");
        // We assume that the option element has only one text child 
        // (it should be this way if the HTML is valid...)
        Text volumeTextTemplate = (Text)volumeOptionTemplate.getFirstChild(); 
                
		fr.imag.clips.papillon.business.dictionary.Volume[] AllVolumes = fr.imag.clips.papillon.business.dictionary.VolumesFactory.getVolumesArray();
                
        for (int i = 0; i < AllVolumes.length; i++) {
            String volumeName = AllVolumes[i].getName();
            volumeOptionTemplate.setValue(volumeName);
            volumeOptionTemplate.setLabel(volumeName);
            // Je dois ici mettre un text dans l'OPTION, car les browser PC ne sont pas conformes aux 
            // specs W3C.
            volumeTextTemplate.setData(volumeName);
            volumeOptionTemplate.setSelected(volumeName.equals(selectedVolume));
            volumeSelect.appendChild(volumeOptionTemplate.cloneNode(true));
        }
        volumeSelect.removeChild(volumeOptionTemplate);

		// fromDate
		XHTMLInputElement fromDateElement = content.getElementFromDate();
		fromDateElement.setValue(fromDate);

		// toDate
		XHTMLInputElement toDateElement = content.getElementToDate();
		toDateElement.setValue(toDate);
		
	}
}
