	var url = 'BrowseVolume.po?';

	/*** HTTP request ***/

  var http_request = false;
  var bufferArrayUp = new Array();
  var bufferArrayDown = new Array();
  var firstParams;
  var increaseLimit = false;
  
  function makeFirstRequests() {
  
    bufferArrayUp = new Array();
    bufferArrayDown = new Array();

  	var elev = document.getElementById('elevator');
  	while (elev.firstChild) {
  		elev.removeChild(elev.firstChild);
  	}
  	
  	var parameters = 'VOLUME=' + document.getElementById('VOLUME').value;
  	parameters += '&HEADWORD=' + document.getElementById('HEADWORD').value;
  	parameters += '&STATUS=' + document.getElementById('STATUS').value;
  	if (document.getElementById('LIMIT')) {
  		var limit = parseInt(document.getElementById('LIMIT').value);
  		// The first time, we build a buffer of 3 times the limit
  		var fetchLimit = limit * 2;
  		parameters += '&LIMIT=' + fetchLimit;
  		var firstLimit = limit / 2;
  		document.getElementById('LIMIT').value = firstLimit;
		increaseLimit = true;
  	};
  	makeRequest("up",parameters);
  	firstParams = parameters;
	waitArrayUpDown();
  }
  
  function increaseTable(direction) {
  	 var headword;
     if (increaseLimit) {
     	if (document.getElementById('LIMIT')) {
  			var limit = parseInt(document.getElementById('LIMIT').value);
  			limit = limit * 2;
  			document.getElementById('LIMIT').value = limit;
  		};
  		increaseLimit = false;   				
    }
  	if (direction=="up") {
  		var coupleArray = bufferArrayUp[bufferArrayUp.length-1].split('#,#');
		headword = coupleArray[0];
  	}
  	else {
  		var coupleArray = bufferArrayDown[bufferArrayDown.length-1].split('#,#');
		headword = coupleArray[0];
  	}
  	var parameters = "VOLUME=" + document.getElementById('VOLUME').value;
  	parameters += "&HEADWORD=" + headword;
  	parameters += '&STATUS=' + document.getElementById('STATUS').value;
  	if (document.getElementById('LIMIT')) {
  		parameters += '&LIMIT=' + document.getElementById('LIMIT').value;
  	}
  	makeRequest(direction,parameters);
  	if (direction=="up") {
  	  		waitArrayUp();
  	}
  	else {
  	  		waitArrayDown();
  	}
  }

  function makeRequest(direction,parameters) {
  
  	parameters+= "&DIRECTION=" + direction;
  		
  	try {
   		netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
   	} catch (e) {
    	/*alert("Permission UniversalBrowserRead denied."); */
   	}

    http_request = false;    
    /*@cc_on
         @if (@_jscript_version >= 5)
          try {
              http_request = new ActiveXObject("Msxml2.XMLHTTP");
              } catch (e) {
              try {
                  http_request = new ActiveXObject("Microsoft.XMLHTTP");
                  } catch (E) {
                  http_request = false;
                  }
              } 
         @else
          http_request = false;
         @end @*/

    if (!http_request && typeof XMLHttpRequest != 'undefined') {
		http_request = new XMLHttpRequest();
	}
    if (http_request.overrideMimeType) {
      http_request.overrideMimeType('text/xml');
    }
	// bug with Firefox
    //http_request.setRequestHeader("Content-Type", "text/xml;charset=utf-8");
    if (!http_request) {
      alert('Cannot create XMLHTTP instance');
      return false;
    }
   // it seems to be impossible to pass parameters to the function called by onreadystatechange
    if (direction== 'up') {
     http_request.onreadystatechange = getUpContent;
    }
    else {
     http_request.onreadystatechange = getDownContent;
    }
    http_request.open('GET', url + parameters, true);
    http_request.send(null);
  }

  function getUpContent() {
    if (http_request.readyState == 4) {
      if (http_request.status == 200) {

        var string = http_request.responseText;
      	addElements(document.getElementById('elevator'),string,'up');

      } else {
        alert('There was a problem with the request.');
      }
    }
   }
    
  function getDownContent() {
    if (http_request.readyState == 4) {
      if (http_request.status == 200) {

        var string = http_request.responseText;
      	addElements(document.getElementById('elevator'),string,'down');

      } else {
        alert('There was a problem with the request.');
      }
    }
  }
  
  function addElements(parent, elementsString, direction) {
					
			var rootbegin = "<entries>";
			var rootend = "<\/entries>";
			
			var first = elementsString.indexOf(rootbegin);
			var end = elementsString.indexOf(rootend);
			
			first += rootbegin.length;
			elementsString = elementsString.substring(first, end-1);
			
			var theArray = elementsString.split('#;#');
			
			if (direction=='up') {
				if (bufferArrayUp.length==0) {
					bufferArrayUp = theArray;
				}
				else {
					theArray.concat(bufferArrayUp);
					bufferArrayUp = theArray;
				}
			}
			else {
				if (bufferArrayDown.length==0) {
					bufferArrayDown = theArray;
				}
				else {
					bufferArrayDown.concat(theArray);
				}
			}
			// XML does not work with Safari!
			//var newNode = theDocument.importNode(element, true);
			//alert(newNode.nodeName);

			//for (var i=0; i< elements.length; i++) {
				//alert(elements[i]);
				//var newNode = theDocument.importNode(elements[i]);
				//alert(newNode);
				//parent.appendChild(newNode);
			//}
		}

  
  	/*** Array ***/
  	
		function addArray(array, direction) {
			var element = document.getElementById('elevator');
			var nbMax = array.length;
			if (document.getElementById('LIMIT')) {
  				var limit = parseInt(document.getElementById('LIMIT').value);
  				nbMax = Math.min(array.length,limit);
  			}
			for (var i=0; i<nbMax; i++) {
				var couple = array[i];
				
				var coupleArray = couple.split('#,#');
				var headword = coupleArray[0];
				// when the array is empty, the first headword is the xml declaration
				if (headword.indexOf('<?xml') <0) {
					var br = document.createElement('br');
					var a = document.createElement('a');
				
					var page = 'ConsultExpert.po?LOOKUP=LOOKUP&VOLUME=' + document.getElementById('VOLUME').value + '&handle=' + coupleArray[1];
				
					a.setAttribute('href',page);
					a.setAttribute('target','_blank');
					var text = document.createTextNode(headword);
					a.appendChild(text);
					if (direction=='up') {
						element.insertBefore(br,element.firstChild);
						element.insertBefore(a,element.firstChild);
					}
					else {
						element.appendChild(a);
						element.appendChild(br);
					}
				}
			}
			array = array.splice(0,nbMax);
		}
				
		
		function waitArrayUpDown() {
   			if (bufferArrayUp.length>0){
      			addArray(bufferArrayUp,'up');
      			makeRequest('down',firstParams);
      			waitArrayDown();
  			} 
			else {
      		// THIS IS RECURSIVE!  
      		// WE'LL KEEP SCHEDULING
			// THE CHECK UNTIL bufferArrayUp.length>0
      			setTimeout('waitArrayUpDown()', 10);
   			}
		}

		function waitArrayUp() {
   			if (bufferArrayUp.length>0){
      			addArray(bufferArrayUp,'up');
  			} else {
      		// THIS IS RECURSIVE!  
      		// WE'LL KEEP SCHEDULING
			// THE CHECK UNTIL bufferArrayUp.length>0
      			setTimeout('waitArrayUp()', 10);
   			}
		}

		function waitArrayDown() {
   			if (bufferArrayDown.length>0){
      			addArray(bufferArrayDown,'down');
   			} else {
      		// THIS IS RECURSIVE!  
      		// WE'LL KEEP SCHEDULING
			// THE CHECK UNTIL bufferArrayDown.length>0
      			setTimeout('waitArrayDown()', 10);
   			}
		}
