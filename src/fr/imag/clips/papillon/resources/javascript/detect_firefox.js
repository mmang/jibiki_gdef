function hideGetFirefoxIfAlreadyAvailable() {
    if (isModernBrowser()) {
        // Hide the get firefox div
        firefoxElement = document.getElementById("firefox"); 
        firefoxElement.style.display = "none";
    }
}

function isFirefox() {
    return ((navigator.userAgent.indexOf("Firefox") != -1) ||
            (navigator.userAgent.indexOf("Iceweasel") != -1) ||
            (navigator.userAgent.indexOf("Safari") != -1)
			);
}

function isModernBrowser() {
	// IE 7, mozilla, safari, opera 9
	return (typeof document.body.style.maxHeight != "undefined");
}
