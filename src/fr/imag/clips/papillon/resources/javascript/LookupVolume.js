	var browseUrl = 'BrowseVolume.po?';
	var lookupUrl = 'LookupVolume.po?';

	/*** HTTP request ***/

  var browse_up_http_request = false;
  var browse_down_http_request = false;
  var lookup_http_request = false;
  var bufferArrayUp = new Array();
  var bufferArrayDown = new Array();
  var firstParams;
  var increaseLimit = false;

   	try {
   		netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
   	} catch (e) {
    	/*alert("Permission UniversalBrowserRead denied."); */
   	}
 
  
  function makeFirstRequests() {
  
    var parameters = 'VOLUME=' + document.getElementById('VOLUME').value;
  	parameters += '&HEADWORD=' + document.getElementById('NEWHEADWORD').value;
	  parameters += '&STATUS=' + document.getElementById('STATUS').value;
	  parameters += '&TARGETS=' + document.getElementById('TARGETS').value;
  	
  	lookupDict(parameters);
  	
  	if (document.getElementById('LIMIT')) {
  		var limit = parseInt(document.getElementById('LIMIT').value);
  		// The first time, we build a buffer of 3 times the limit
  		var fetchLimit = limit * 2;
  		parameters += '&LIMIT=' + fetchLimit;
  		//var firstLimit = limit / 2;
  		//document.getElementById('LIMIT').value = firstLimit;
		//increaseLimit = true;
  	}
  
    bufferArrayUp = new Array();
    bufferArrayDown = new Array();

  	var elev = document.getElementById('lift');
  	while (elev.firstChild) {
  		elev.removeChild(elev.firstChild);
  	}
  	
  	makeRequest("up",parameters);
  	firstParams = parameters;
	waitArrayUpDown();
  }
    
  function increaseTable(direction) {
  	 var headword;
  	if (direction=="up") {
  		var coupleArray = bufferArrayUp[bufferArrayUp.length-1].split('#,#');
		headword = coupleArray[0];
  	}
  	else {
  		var coupleArray = bufferArrayDown[bufferArrayDown.length-1].split('#,#');
		headword = coupleArray[0];
  	}
  	var parameters = "VOLUME=" + document.getElementById('VOLUME').value;
  	parameters += "&HEADWORD=" + headword;
  	parameters += '&STATUS=' + document.getElementById('STATUS').value;
  	if (document.getElementById('LIMIT')) {
  		parameters += '&LIMIT=' + document.getElementById('LIMIT').value;
  	}
  	makeRequest(direction,parameters);
  	if (direction=="up") {
  	  		waitArrayUp();
  	}
  	else {
  	  		waitArrayDown();
  	}
  }
  
  function lookupDict(parameters) {
  	lookup_http_request = createHttpRequest();
    lookup_http_request.onreadystatechange = displayEntries;
    lookup_http_request.open('GET', lookupUrl + parameters, true);
    lookup_http_request.send(null);
  }
  
  function displayEntries() {
    if (lookup_http_request.readyState == 4) {
      if (lookup_http_request.status == 200) {
		  var responseHead = lookup_http_request.responseText.substring(0,80);
		  if (responseHead.match('fuzzySearch')!== null) {
			  document.getElementById('fuzzySearch').setAttribute('style','');
		  }
		  else {
			  document.getElementById('fuzzySearch').setAttribute('style','display:none;');
		  }
		  if (responseHead.match('emptySearch')!==null) {
			  document.getElementById('emptySearch').setAttribute('style','');
		  }
		  else {
			  document.getElementById('emptySearch').setAttribute('style','display:none;');
		  }
		  document.getElementById('entriesHolder').innerHTML=lookup_http_request.responseText;
      } else {
        alert('There was a problem with the request.');
      }
    }
  }


  function makeRequest(direction,parameters) {
  
  	parameters+= "&DIRECTION=" + direction;
  	  		
   // it seems to be impossible to pass parameters to the function called by onreadystatechange
    if (direction== 'up') {
     browse_up_http_request= createHttpRequest();
     browse_up_http_request.onreadystatechange = getUpContent;
     browse_up_http_request.open('GET', browseUrl + parameters, true);
     browse_up_http_request.send(null);
    }
    else {
     browse_down_http_request = createHttpRequest();
     browse_down_http_request.onreadystatechange = getDownContent;
     browse_down_http_request.open('GET', browseUrl + parameters, true);
     browse_down_http_request.send(null);
    }
  }
  
  function createHttpRequest () {
     var new_http_request = false;    
    /*@cc_on
         @if (@_jscript_version >= 5)
          try {
              new_http_request = new ActiveXObject("Msxml2.XMLHTTP");
              } catch (e) {
              try {
                  new_http_request = new ActiveXObject("Microsoft.XMLHTTP");
                  } catch (E) {
                  new_http_request = false;
                  }
              } 
         @else
          new_http_request = false;
         @end @*/

    if (!new_http_request && typeof XMLHttpRequest != 'undefined') {
		new_http_request = new XMLHttpRequest();
	}
    if (new_http_request.overrideMimeType) {
      new_http_request.overrideMimeType('text/xml');
    }
	// bug with Firefox
    //new_http_request.setRequestHeader("Content-Type", "text/xml;charset=utf-8");
    if (!new_http_request) {
      alert('Cannot create XMLHTTP instance');
      return false;
    }
	else {
	  return new_http_request;
	}
  }
  

  function getUpContent() {
    if (browse_up_http_request.readyState == 4) {
      if (browse_up_http_request.status == 200) {

        var string = browse_up_http_request.responseText;
      	addElements(document.getElementById('lift'),string,'up');

      } else {
        alert('There was a problem with the request.');
      }
    }
   }
    
  function getDownContent() {
    if (browse_down_http_request.readyState == 4) {
      if (browse_down_http_request.status == 200) {

        var string = browse_down_http_request.responseText;
      	addElements(document.getElementById('lift'),string,'down');

      } else {
        alert('There was a problem with the request.');
      }
    }
  }
  
  function addElements(parent, elementsString, direction) {
					
			var rootbegin = "<entries>";
			var rootend = "<\/entries>";
			
			var first = elementsString.indexOf(rootbegin);
			var end = elementsString.indexOf(rootend);
			
			var first = first + rootbegin.length;
			elementsString = elementsString.substring(first, end-3);
			
			var theArray = new Array();
			theArray = elementsString.split('#;#');
			
			if (direction=='up') {
				if (bufferArrayUp.length==0) {
					bufferArrayUp = theArray;
				}
				else {
					theArray.concat(bufferArrayUp);
					bufferArrayUp = theArray;
				}
			}
			else {
				if (bufferArrayDown.length==0) {
					bufferArrayDown = theArray;
				}
				else {
					bufferArrayDown.concat(theArray);
				}
			}
			// XML does not work with Safari!
			//var newNode = theDocument.importNode(element, true);
			//alert(newNode.nodeName);

			//for (var i=0; i< elements.length; i++) {
				//alert(elements[i]);
				//var newNode = theDocument.importNode(elements[i]);
				//alert(newNode);
				//parent.appendChild(newNode);
			//}
		}

  
  	/*** Array ***/
  	
		function addArray(array, direction) {
			var element = document.getElementById('lift');
			var nbMax = array.length;
			if (document.getElementById('LIMIT')) {
				var limit = parseInt(document.getElementById('LIMIT').value);
  				nbMax = Math.min(array.length,limit);
  			}
			var origHeadword = document.getElementById('NEWHEADWORD').value;
			for (var i=0; i<nbMax; i++) {
				var couple = array[i];
				
				var coupleArray = couple.split('#,#');
				var headword = coupleArray[0];
				// when the array is empty, the first headword is the xml declaration
				if (headword.indexOf('<?xml') <0) {
					var br = document.createElement('br');
					var a = document.createElement('a');
									
					//var pageHref = 'ConsultExpert.po?LOOKUP=LOOKUP&VOLUME=' + document.getElementById('VOLUME').value + '&HANDLE=' + coupleArray[1];
					//var pageOnclick = 'lookupDict(\'' + 'VOLUME=' + document.getElementById('VOLUME').value + '&HANDLE=' + coupleArray[1] + '\');document.getElementById(\'NEWHEADWORD\').value=\''+ headword + '\';makeFirstRequests();return false;';
					//var pageHref = 'javascript:lookupDict(\'' + 'VOLUME=' + document.getElementById('VOLUME').value + '&amp;HANDLE=' + coupleArray[1] + '\');boldify(this);unboldify(document.getElementById(\'lift\'));return false;';
					var pageOnclick = 'lookupDict(\'VOLUME=' + document.getElementById('VOLUME').value + '&HANDLE=' + coupleArray[1] + '\');unboldify(this.parentNode);boldify(this);return false;';
					//var pageHref = 'javascript:boldify(this);return false;';
				
					a.setAttribute('href','javascript:void(0)');
					a.setAttribute('onclick',pageOnclick);
					//a.setAttribute('onclick',pageOnclick);
					//a.setAttribute('target','_blank');
					var text = document.createTextNode(headword);
					a.appendChild(text);
					if (origHeadword == headword) {
						boldify(a);
					}
					if (direction=='up') {
						element.insertBefore(br,element.firstChild);
						element.insertBefore(a,element.firstChild);
					}
					else {
						element.appendChild(a);
						element.appendChild(br);
					}
				}
			}
			array = array.splice(0,nbMax);
		}
		
	function boldify (element) {
		element.setAttribute('style','font-weight:bold;font-size:larger;');
		if (element.style.setAttribute)  {
			element.style.setAttribute('style','font-weight:bold;font-size:larger;');						
		}
	}

	function unboldify (array) {
		array = array.childNodes;
		var len=array.length;
		for (var i=0; i<len; i++) {
			var element = array[i];
			element.removeAttribute('style');
		}
	}

function waitArrayUpDown() {
   			if (bufferArrayUp.length>0){
      			addArray(bufferArrayUp,'up');
      			makeRequest('down',firstParams);
      			waitArrayDown();
  			} 
			else {
      		// THIS IS RECURSIVE!  
      		// WE'LL KEEP SCHEDULING
			// THE CHECK UNTIL bufferArrayUp.length>0
      			setTimeout('waitArrayUpDown()', 10);
   			}
		}

		function waitArrayUp() {
   			if (bufferArrayUp.length>0){
      			addArray(bufferArrayUp,'up');
  			} else {
      		// THIS IS RECURSIVE!  
      		// WE'LL KEEP SCHEDULING
			// THE CHECK UNTIL bufferArrayUp.length>0
      			setTimeout('waitArrayUp()', 10);
   			}
		}

		function waitArrayDown() {
   			if (bufferArrayDown.length>0){
      			addArray(bufferArrayDown,'down');
   			} else {
      		// THIS IS RECURSIVE!  
      		// WE'LL KEEP SCHEDULING
			// THE CHECK UNTIL bufferArrayDown.length>0
      			setTimeout('waitArrayDown()', 10);
   			}
		}

function adjustConsultExpertForm(select) {
	if (selectedValue(select)=='gdef-est-domaine') {
		if (select.id=='search1') {
			document.getElementById('domaine1').value=document.getElementById('search1text').value;
			document.getElementById('search1text').setAttribute('name','toto1');
			document.getElementById('search1text').setAttribute('style','display:none;');
			document.getElementById('domaine1').setAttribute('name','search1text');
			document.getElementById('domaine1').setAttribute('style','');
			document.getElementById('Strategy1').value=4;
			document.getElementById('Strategy1').setAttribute('style','display:none;');
			document.getElementById('Strategy1Default').setAttribute('style','');
		}
		else {
			document.getElementById('domaine2').value=document.getElementById('search2text').value;
			document.getElementById('search2text').setAttribute('name','toto2');
			document.getElementById('search2text').setAttribute('style','display:none;');
			document.getElementById('domaine2').setAttribute('name','search2text');
			document.getElementById('domaine2').setAttribute('style','');
			document.getElementById('Strategy2').value=4;
			document.getElementById('Strategy2').setAttribute('style','display:none;');
			document.getElementById('Strategy2Default').setAttribute('style','');
		}
	}
	else if (selectedValue(select)=='cdm-translation-ref') {
		if (select.id=='search1') {
			document.getElementById('Strategy1').value=5;
			document.getElementById('Strategy1Default').setAttribute('style','');
			document.getElementById('Strategy1').setAttribute('style','display:none;');
			document.getElementById('search1text').setAttribute('name','search1text');
			document.getElementById('search1text').setAttribute('style','');
			document.getElementById('domaine1').setAttribute('name','domaine1');
			document.getElementById('domaine1').setAttribute('style','display:none;');
		}
		else {
			document.getElementById('Strategy2').value=5;
			document.getElementById('Strategy2Default').setAttribute('style','');
			document.getElementById('Strategy2').setAttribute('style','display:none;');
			document.getElementById('search2text').setAttribute('name','search2text');
			document.getElementById('search2text').setAttribute('style','');
			document.getElementById('domaine2').setAttribute('name','domaine2');
			document.getElementById('domaine2').setAttribute('style','display:none;');
		}
	}
	else {
		if (select.id=='search1') {
			document.getElementById('Strategy1Default').setAttribute('style','display:none;');
			document.getElementById('Strategy1').setAttribute('style','');
			document.getElementById('search1text').setAttribute('style','');
			document.getElementById('search1text').setAttribute('name','search1text');
			document.getElementById('domaine1').setAttribute('name','domaine1');
			document.getElementById('domaine1').setAttribute('style','display:none;');
		}
		else {
			document.getElementById('Strategy2Default').setAttribute('style','display:none;');
			document.getElementById('Strategy2').setAttribute('style','');
			document.getElementById('search2text').setAttribute('style','');
			document.getElementById('search2text').setAttribute('name','search2text');
			document.getElementById('domaine2').setAttribute('name','domaine2');
			document.getElementById('domaine2').setAttribute('style','display:none;');
		}
	}
}

function validConsultExpertForm(form) { 
	handle = document.getElementById('HANDLE');
	if (handle != null) {
		handle.parentNode.removeChild(handle);
	}
	volume = document.getElementById('VOLUME');
	if (volume != null) {
		volume.parentNode.removeChild(volume);
	}
	if (form.search1.value=='cdm-translation-ref') {
		form.search1text.value='fra.'+form.search1text.value+'.';
	}
	if (form.search2.value=='cdm-translation-ref') {
		form.search2text.value='fra.'+form.search2text.value+'.';
	}
	return true;
}

function selectedValue(selBox) {
	return selBox.options[selBox.selectedIndex].value;
}
