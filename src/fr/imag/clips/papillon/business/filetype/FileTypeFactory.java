 /*
 * $Id: FileTypeFactory.java 521 2006-08-20 14:57:19Z mangeot $
 *-----------------------------------------------
 * $Log$
 * Revision 1.1.1.1.12.1  2006/08/20 14:57:19  mangeot
 * *** empty log message ***
 *
 * Revision 1.1.1.1  2004/12/06 16:38:31  serasset
 * Papillon for enhydra 5.1. This version compiles and starts with enhydra 5.1.
 * There are still bugs in the code.
 *
 * Revision 1.1.1.1  2002/10/28 16:49:14  serasset
 * Creation of the papillon CVS repository for enhydra 5.0
 *
 * Revision 1.2  2001/08/20 15:57:28  salvati
 * Conversion de lien marche corectement.
 *
 * Revision 1.1  2001/08/07 13:12:57  salvati
 * Added in cvs entries.
 *
 */
 
package fr.imag.clips.papillon.business.filetype;
 
public class FileTypeFactory{
    
    public static String[] getFileTypeList()
    {
    String[] theList=new String[4];
    theList[0]="texte";
    theList[1]="html";
    theList[2]="xml";
    theList[3]="tar";    
    return theList;
    }
 }
