<xsl:stylesheet
   xmlns="http://www.w3.org/1999/xhtml"
  xmlns:d="http://www-clips.imag.fr/geta/services/dml"
  xmlns:g="http://www.estfra.ee/~gdef/xmlschema"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xml="http://www.w3.org/XML/1998/namespace"    exclude-result-prefixes="g d xsl xsi"    version="1.0">
  <xsl:output    encoding="utf-8"    indent="no"    method="xml"/>

<!-- general templates please do not modify -->

<!-- here I do not use the xsl:copy because it copies also xmlns: attributes -->
  <xsl:template    match="g:*"    priority="-1">
    <xsl:element    name="d:{local-name()}"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:apply-templates    select="@*|*|text()"/>
    </xsl:element>
  </xsl:template>
  <xsl:template    match="@*">
    <xsl:copy/>
  </xsl:template>
  <xsl:template    match="comment()">
    <xsl:comment>
      <xsl:value-of    select="."/>
    </xsl:comment>
  </xsl:template>
  <xsl:template    match="/">
    <div>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

<!-- templates spécifiques aux métadonnées -->
  <xsl:template    match="g:volume">
    <xsl:apply-templates    select="*"/>
  </xsl:template>
  <xsl:template    match="d:contribution">
    <xsl:copy>
      <xsl:apply-templates    select="d:metadata"/>
      <xsl:apply-templates    select="d:data"/>
    </xsl:copy>
  </xsl:template>

<!-- xsl:template match="d:metadata"><xsl:apply-templates select="d:history"/></xsl:template -->

<!-- xsl:template match="d:metadata"></xsl:template -->
  <xsl:template    match="d:metadata">
    <xsl:apply-templates    select="d:status"/>
  </xsl:template>
  <xsl:template    match="d:status">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template    match="d:history">
    <xsl:for-each    select="d:modification">
      <xsl:if    test="d:author/text()!=''">
        <d:comment>Modif par
          <xsl:apply-templates    select="d:author"/>, le
          <xsl:apply-templates    select="d:date"/>:
          <xsl:apply-templates    select="d:comment"/>
        </d:comment>
        <xsl:text/>
      </xsl:if>
    </xsl:for-each>
    <xsl:for-each    select="modification">
      <xsl:if    test="author/text()!=''">
        <d:comment>Modif par
          <xsl:apply-templates    select="author"/>,le
          <xsl:apply-templates    select="date"/>:
          <xsl:apply-templates    select="comment"/>
        </d:comment>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
  <xsl:template    match="d:modification">
    <xsl:if    test="d:author/text()!=''">
      <d:comment>Modif par
        <xsl:apply-templates    select="d:author"/>, le
        <xsl:apply-templates    select="d:date"/>:
        <xsl:apply-templates    select="d:comment"/>
      </d:comment>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="modification">
    <xsl:if    test="author/text()!=''">
      <d:comment>Modif par
        <xsl:apply-templates    select="author/text()"/>, le
        <xsl:apply-templates    select="date/text()"/>:
        <xsl:apply-templates    select="comment/text()"/>
      </d:comment>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:author">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:date">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:comment">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:data">
    <xsl:apply-templates    select="*"/>
  </xsl:template>

<!-- templates spécifiques au dico -->

<!-- convertit les balises à la BBCode du texte en balises xml -->
  <xsl:template    match="g:avant">
    <xsl:element    name="d:avant"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:call-template    name="convert-all-tags">
        <xsl:with-param    name="input"    select="text()"/>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

<!-- convertit les balises à la BBCode du texte en balises xml -->
  <xsl:template    match="g:apres">
    <xsl:element    name="d:apres"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:call-template    name="convert-all-tags">
        <xsl:with-param    name="input"    select="text()"/>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

<!-- convertit les balises à la BBCode du texte en balises xml -->
  <xsl:template    match="g:exemple-est">
    <xsl:element    name="d:exemple-est"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:call-template    name="convert-all-tags">
        <xsl:with-param    name="input"    select="text()"/>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

<!-- convertit les balises à la BBCode du texte en balises xml -->
  <xsl:template    match="g:explication">
    <xsl:element    name="d:explication"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:call-template    name="convert-all-tags">
        <xsl:with-param    name="input"    select="text()"/>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

<!-- convertit les balises à la BBCode du texte en balises xml -->
  <xsl:template    match="g:traduction-exe">
    <xsl:element    name="d:traduction-exe"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:call-template    name="convert-all-tags">
        <xsl:with-param    name="input"    select="text()"/>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

<!-- convertit les balises à la BBCode du texte en balises xml -->
  <xsl:template    match="g:expression-est">
    <xsl:element    name="d:expression-est"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:call-template    name="convert-all-tags">
        <xsl:with-param    name="input"    select="text()"/>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

<!-- convertit les balises à la BBCode du texte en balises xml -->
  <xsl:template    match="g:traduction-expr">
    <xsl:element    name="d:traduction-expr"    namespace="http://www-clips.imag.fr/geta/services/dml">
      <xsl:call-template    name="convert-all-tags">
        <xsl:with-param    name="input"    select="text()"/>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

<!-- template convertissant les balises [b] [/b] (à la BBCode) en balises xml -->
  <xsl:template    name="convert-all-tags">
    <xsl:param    name="input"/>
    <xsl:param    name="opentagchar">[</xsl:param>
    <xsl:param    name="closetagchar">]</xsl:param>
    <xsl:param    name="endtagchar">/</xsl:param>
    <xsl:choose>
      <xsl:when    test="contains($input,$opentagchar) and contains(substring-after($input,$opentagchar),$closetagchar)">
        <xsl:variable    name="tag">
          <xsl:value-of    select="substring-before(substring-after($input,$opentagchar),$closetagchar)"/>
        </xsl:variable>
        <xsl:variable    name="starttag">
          <xsl:value-of    select="$opentagchar"/>
          <xsl:value-of    select="$tag"/>
          <xsl:value-of    select="$closetagchar"/>
        </xsl:variable>
        <xsl:variable    name="endtag">
          <xsl:value-of    select="$opentagchar"/>
          <xsl:value-of    select="$endtagchar"/>
          <xsl:value-of    select="$tag"/>
          <xsl:value-of    select="$closetagchar"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when    test="contains(substring-after($input,$starttag),$endtag)">
            <xsl:call-template    name="convert-one-tag">
              <xsl:with-param    name="input">
                <xsl:value-of    select="substring-before($input,$endtag)"/>
                <xsl:value-of    select="$endtag"/>
              </xsl:with-param>
              <xsl:with-param    name="tag"    select="$tag"/>
              <xsl:with-param    name="opentag"    select="$starttag"/>
              <xsl:with-param    name="closetag"    select="$endtag"/>
            </xsl:call-template>
            <xsl:call-template    name="convert-all-tags">
              <xsl:with-param    name="input"    select="substring-after($input,$endtag)"/>
              <xsl:with-param    name="opentagchar"    select="$opentagchar"/>
              <xsl:with-param    name="closetagchar"    select="$closetagchar"/>
              <xsl:with-param    name="endtagchar"    select="$endtagchar"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of    select="substring-before($input,$starttag)"/>
            <xsl:value-of    select="$starttag"/>
            <xsl:call-template    name="convert-all-tags">
              <xsl:with-param    name="input"    select="substring-after($input,$starttag)"/>
              <xsl:with-param    name="opentagchar"    select="$opentagchar"/>
              <xsl:with-param    name="closetagchar"    select="$closetagchar"/>
              <xsl:with-param    name="endtagchar"    select="$endtagchar"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of    select="$input"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

<!-- template convertissant une balise spécifique [b] [/b] (à la BBCode) en balises xml -->
  <xsl:template    name="convert-one-tag">
    <xsl:param    name="input"/>
    <xsl:param    name="tag">i</xsl:param>
    <xsl:param    name="opentag">[
      <xsl:value-of    select="$tag"/>]
    </xsl:param>
    <xsl:param    name="closetag">[/
      <xsl:value-of    select="$tag"/>]
    </xsl:param>
    <xsl:choose>
      <xsl:when    test="contains($input,$opentag) and contains($input,$closetag)">
        <xsl:value-of    select="substring-before($input,$opentag)"/>
        <xsl:element    name="{$tag}">
          <xsl:value-of    select="substring-after(substring-before($input,$closetag),$opentag)"/>
        </xsl:element>
        <xsl:call-template    name="convert-one-tag">
          <xsl:with-param    name="input"    select="substring-after($input,$closetag)"/>
          <xsl:with-param    name="tag"    select="$tag"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of    select="$input"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
