<xsl:stylesheet
   xmlns="http://www.w3.org/1999/xhtml"
  xmlns:d="http://www-clips.imag.fr/geta/services/dml"
  xmlns:g="http://www.estfra.ee/~gdef/xmlschema"
  xmlns:xml="http://www.w3.org/XML/1998/namespace"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    exclude-result-prefixes="d g xsi xsl"    version="1.0">
  <xsl:output    encoding="utf-8"    indent="no"    method="xml"/>

<!-- general templates please do not modify -->

<!-- here I do not use the xsl:copy because it copies also xmlns: attributes -->

<!-- xsl:template match="*" priority="-1"> <xsl:element name="{name()}"><xsl:apply-templates select="@*|*|text()"/> </xsl:element> </xsl:template -->
  <xsl:template    match="*"    priority="-1">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template    match="@*">
    <xsl:copy/>
  </xsl:template>
  <xsl:template    match="comment()">
    <xsl:comment>
      <xsl:value-of    select="."/>
    </xsl:comment>
  </xsl:template>
  <xsl:template    match="/">
    <xsl:apply-templates/>
  </xsl:template>

<!-- comtribution templates -->
  <xsl:template    match="d:contribution">
    <xsl:apply-templates    select="d:article"/>
  </xsl:template>

<!-- volume specific templates -->
  <xsl:template    match="d:article">
    <d:entry>
      <xsl:apply-templates    select="d:vedette"/>
      <xsl:apply-templates    select="d:bloc-gram"/>
      <xsl:apply-templates    select="d:bloc-phraseol"/>
    </d:entry>
  </xsl:template>

<!-- ajouts pour les traductions françaises -->
  <xsl:template    match="d:vedette">
    <xsl:apply-templates    select="d:particule"/>
    <xsl:apply-templates    select="d:h-aspire"/>
    <xsl:apply-templates    select="d:mot"/>
    <xsl:apply-templates    select="d:hm"/>
    <xsl:if    test="d:variante/text()!=''">
      <xsl:apply-templates    select="d:variante"/>
    </xsl:if>
    <xsl:apply-templates    select="d:type"/>
    <xsl:apply-templates    select="d:bloc-morph"/>
    <xsl:if    test="d:registre-vedette/text()!='' or d:domaine-vedette/text()!=''">
      <br/>
      <d:space/>
      <d:space/>
      <d:space/>
    </xsl:if>
    <xsl:apply-templates    select="d:registre-vedette"/>
    <xsl:apply-templates    select="d:domaine-vedette"/>
    <xsl:apply-templates    select="d:prononciation"/>
    <xsl:apply-templates    select="d:grammaire"/>
  </xsl:template>
  <xsl:template    match="d:particule">
    <xsl:if    test="text()!=''">
      <d:headword>
        <xsl:apply-templates/>
      </d:headword>
      <d:space/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:mot">
    <d:headword>
      <xsl:apply-templates/>
    </d:headword>
  </xsl:template>
  <xsl:template    match="d:hm">
    <xsl:if    test="text()!=''">
      <sup>
        <xsl:apply-templates/>
      </sup>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:variante">
    <xsl:if    test="text()!=''">=
      <d:headword>
        <xsl:apply-templates/>
      </d:headword>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:type">
    <d:space/>
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:bloc-morph">
    <xsl:if    test="d:flexion/text()!='' or d:formes/text()!=''">
      <br/>
      <d:space/>
      <d:space/>
      <d:space/>
    </xsl:if>
    <xsl:apply-templates    select="d:flexion"/>
    <xsl:apply-templates    select="d:formes"/>
  </xsl:template>
  <xsl:template    match="d:flexion">
    <xsl:if    test="text()!=''">
      <xsl:text>[</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>]</xsl:text>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:formes">
    <xsl:if    test="text()!=''">
      <d:space/>
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:registre-vedette">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:domaine-vedette">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:bloc-gram">
    <d:sense>
      <xsl:if    test="count(../d:bloc-gram) >1">
        <b>
          <xsl:number    count="//d:article/d:bloc-gram"    format="I. "    level="single"/>
        </b>
      </xsl:if>
      <xsl:apply-templates    select="*"/>
    </d:sense>
  </xsl:template>
  <xsl:template    match="d:cat-gram">
    <b    style="color:black">
      <d:pos>
        <xsl:apply-templates/>
      </d:pos>
    </b>
  </xsl:template>
  <xsl:template    match="d:registre-bloc-gram">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:domaine-bloc-gram">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>

<!-- xsl:if test="text()!=''"> <a> <xsl:attribute name="href"> <xsl:value-of select="text()"/> </xsl:attribute> <xsl:apply-templates/> </a> </xsl:if -->

<!-- On ne met pas encore les liens en place car il manque des mots-vedette -->
  <xsl:template    match="d:renvoi-vedette">
    <xsl:if    test="text()!=''">
      <d:space/>
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:bloc-semantique">
    <d:sense>
      <xsl:if    test="count(../d:bloc-semantique) >1">
        <b>
          <xsl:number    count="//d:article/d:bloc-gram/d:bloc-semantique"    format="1. "    level="single"/>
        </b>
      </xsl:if>
      <xsl:apply-templates    select="d:indication-sem1"/>
      <xsl:apply-templates    select="d:registre-sens-ved"/>
      <xsl:apply-templates    select="d:domaine-bloc-semantique"/>
      <xsl:apply-templates    select="d:sous-bloc-semantique"/>
      <xsl:apply-templates    select="d:bloc-exemples"/>
    </d:sense>
  </xsl:template>
  <xsl:template    match="d:indication-sem1">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <i>
          <xsl:apply-templates/>
        </i>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:registre-sens-ved">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:domaine-bloc-semantique">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:sous-bloc-semantique">
    <d:sense>
      <xsl:if    test="count(../d:sous-bloc-semantique) >1">
        <b>
          <xsl:number    count="//d:article/d:bloc-gram/d:bloc-semantique/d:sous-bloc-semantique"    format="1. "    level="single"/>
        </b>
      </xsl:if>
      <xsl:apply-templates    select="d:indication-sem2"/>
      <xsl:apply-templates    select="d:bloc-contextuel"/>
    </d:sense>
  </xsl:template>
  <xsl:template    match="d:indication-sem2">
    <xsl:if    test="text()!=''">
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:bloc-contextuel">
    <d:sense>
      <xsl:apply-templates    select="*"/>
    </d:sense>
  </xsl:template>
  <xsl:template    match="d:indication-cont">
    <xsl:if    test="text()!=''">
      <d:label>
        <span    style="font-size : smaller;">
          <xsl:apply-templates/>
        </span>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:bloc-equivalent">
    <d:sense>
      <xsl:apply-templates    select="*"/>
    </d:sense>
  </xsl:template>
  <xsl:template    match="d:avant">
    <xsl:if    test="text()!=''">
      <d:fra>
        <xsl:apply-templates/>
      </d:fra>
      <d:space/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:mot-princ">

<!-- pb si on met fra, tout l'article français sera en bleu. Comment faire ? -->

<!-- d:fra -->
    <d:translation>
      <xsl:apply-templates    select=".//d:h-aspire"/>
      <xsl:choose>
        <xsl:when    test=".//d:status/text()!='validated' and .//d:mot/text()!=''">
          <span    style="text-decoration:underline">
            <d:fra>
              <xsl:apply-templates    select=".//d:mot"/>
            </d:fra>
          </span>
        </xsl:when>
        <xsl:when    test=".//d:status/text()='validated' and .//d:mot/text()!=''">
          <d:fra>
            <xsl:apply-templates    select=".//d:mot"/>
          </d:fra>
        </xsl:when>
        <xsl:otherwise/>
      </xsl:choose>
      <xsl:apply-templates    select=".//d:prononciation"/>
      <xsl:apply-templates    select=".//d:grammaire"/>
      <xsl:apply-templates    select="text()"/>
    </d:translation>

<!-- /d:fra -->
  </xsl:template>
  <xsl:template    match="d:apres">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:fra>
        <xsl:apply-templates/>
      </d:fra>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:registre-equiv">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:rection-equiv">
    <xsl:if    test="d:rection-equiv-est/text()!='' or d:rection-equiv-fra/text()!=''">
      <d:space/>[
      <xsl:apply-templates    select="*"/>]
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:rection-trad">
    <xsl:if    test="d:rection-trad-est/text()!='' or d:rection-trad-fra/text()!=''">
      <d:space/>[
      <xsl:apply-templates    select="*"/>]
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:rection-equiv-est|d:rection-trad-est">
    <xsl:if    test="text()!=''">
      <d:est>
        <xsl:apply-templates/>
      </d:est>
      <d:space/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:rection-equiv-fra|d:rection-trad-fra">
    <xsl:if    test="text()!=''">
      <d:fra>
        <xsl:apply-templates/>
      </d:fra>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:explication">
    <xsl:if    test="text()!=''">
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:bloc-exemples">
    <xsl:if    test="d:exemple/d:exemple-est/text()!='' or count(d:exemple/d:exemple-est/*) > 0">
      <d:exemples>
        <xsl:text>Exemples :</xsl:text>
        <xsl:apply-templates/>
      </d:exemples>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:proverbe-exemple-est">
    <br/>
    <xsl:if    test="text()='true'">
      <xsl:text>vs.</xsl:text>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:exemple">
    <xsl:if    test="position()!=1">
      <br/>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:registre-exe-est">
    <xsl:if    test="text()!=''">
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:domaine-exe-est">
    <xsl:if    test="text()!=''">
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:exemple-est">
    <d:est>
      <d:example>
        <xsl:apply-templates/>
      </d:example>
    </d:est>
  </xsl:template>
  <xsl:template    match="d:bloc-traduction-exe">
    <xsl:if    test="d:traduction-exe/text()!='' or count(d:traduction-exe/*) > 0">
      <br/>
      <xsl:if    test="count(../d:bloc-traduction-exe) >1">
        <b>
          <xsl:number    count="//d:article/d:bloc-gram/d:bloc-semantique/d:bloc-exemples/d:exemple/d:bloc-traduction-exe"    format="1. "    level="single"/>
        </b>
      </xsl:if>
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:indic-sem-exe|d:indic-sem-expr">
    <xsl:if    test="text()!=''">
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:domaine-exe">
    <xsl:if    test="text()!=''">
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:proverbe-traduction-exe">
    <xsl:if    test="text()='true'">
      <xsl:text>vs.</xsl:text>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:traduction-exe">
    <d:fra>
      <d:example>
        <xsl:apply-templates/>
      </d:example>
    </d:fra>
  </xsl:template>
  <xsl:template    match="d:bloc-phraseol">
    <xsl:if    test="d:unite-phraseol/d:expression-est/text()!='' or count(d:unite-phraseol/d:expression-est/*) >0">
      <d:idioms>
        <xsl:text>Phraséologie :</xsl:text>
        <xsl:apply-templates/>
      </d:idioms>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:unite-phraseol">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:expression-est">
    <xsl:if    test="text()!=''">
      <br/>
      <d:est>
        <d:idiom>
          <xsl:apply-templates/>
        </d:idiom>
      </d:est>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:registre-expr-est">
    <xsl:if    test="text()!=''">
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:bloc-traduction-expr">
    <xsl:if    test="d:traduction-expr/text()!='' or count(d:traduction-expr/*) >0">
      <br/>
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:locution-traduction-expr">
    <xsl:if    test="text()='true'">
      <xsl:text>välj.</xsl:text>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:traduction-expr">
    <xsl:if    test="text()!=''">
      <d:fra>
        <d:idiom>
          <xsl:apply-templates/>
        </d:idiom>
      </d:fra>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:registre-trad-exe|d:registre-trad-expr">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:label>
        <xsl:apply-templates/>
      </d:label>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:bloc-renvois">
    <xsl:if    test="d:renvoi-phraseol/text()!=''">
      <d:sense>
        <xsl:text>Renvois :</xsl:text>
        <d:space/>
        <xsl:for-each    select="d:renvoi-phraseol">
          <a>
            <xsl:attribute    name="href">
              <xsl:value-of    select="text()"/>
            </xsl:attribute>
            <xsl:apply-templates/>
          </a>
          <xsl:if    test="position()!=last()">
            <xsl:text>,</xsl:text>
            <d:space/>
          </xsl:if>
        </xsl:for-each>
      </d:sense>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:renvoi-phraseol">
    <a>
      <xsl:attribute    name="href">
        <xsl:value-of    select="text()"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </a>
    <xsl:if    test="position()!=last()">
      <xsl:text>,</xsl:text>
      <d:space/>
    </xsl:if>
  </xsl:template>

<!-- templates pour la traduction française -->
  <xsl:template    match="d:h-aspire">
    <xsl:if    test="text()='true'">
      <xsl:text>*</xsl:text>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:prononciation">
    <xsl:if    test="text()!=''">
      <d:space/>
      <d:meta>
        <d:pronunciation>
          <xsl:apply-templates/>
        </d:pronunciation>
      </d:meta>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:grammaire">
    <xsl:apply-templates    select="d:genre-nbr"/>
    <xsl:if    test="d:pluriel-irr/text()!='' or d:feminin-irr/text()!=''">
      <xsl:text>(</xsl:text>
      <xsl:apply-templates    select="d:pluriel-irr"/>
      <xsl:if    test="d:pluriel-irr/text()!='' and d:feminin-irr/text()!=''">
        <xsl:text>,</xsl:text>
      </xsl:if>
      <xsl:apply-templates    select="d:feminin-irr"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:apply-templates    select="d:conjugaison"/>
    <xsl:apply-templates    select="d:aux-etre"/>
  </xsl:template>
  <xsl:template    match="d:genre-nbr">
    <xsl:if    test="text()!='' and text()!=' '">
      <d:space/>
      <i>
        <xsl:apply-templates/>
      </i>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:pluriel-irr">
    <xsl:if    test="text()!=''">
      <xsl:text>pl.</xsl:text>
      <d:fra>
        <xsl:apply-templates/>
      </d:fra>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:feminin-irr">
    <xsl:if    test="text()!=''">
      <xsl:text>f.</xsl:text>
      <d:fra>
        <xsl:apply-templates/>
      </d:fra>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:conjugaison">
    <xsl:if    test="text()!=''">
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:aux-etre">
    <xsl:if    test="text()='true'">
      <i>
        <xsl:text>ê</xsl:text>
      </i>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
