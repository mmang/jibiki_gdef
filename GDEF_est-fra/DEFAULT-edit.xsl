<xsl:stylesheet
   xmlns="http://www.w3.org/1999/xhtml"
  xmlns:d="http://www-clips.imag.fr/geta/services/dml"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xml="http://www.w3.org/XML/1998/namespace"    exclude-result-prefixes="d xlink xsl xsi"    version="1.0">
  <xsl:output    encoding="utf-8"    indent="yes"    method="xml"/>

<!-- Copy through any element that doesn't have a template. -->

<!-- Include attributes and content. -->
  <xsl:template    match="*|@*"    priority="-2">
    <xsl:copy>
      <xsl:apply-templates    select="@*|*|text()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template    match="comment()">
    <xsl:comment>
      <xsl:value-of    select="."/>
    </xsl:comment>
  </xsl:template>

<!-- transform any dml element into a span -->
  <xsl:template    match="d:*"    priority="-1">
    <input    type="checkbox">
      <xsl:attribute    name="name">
        <xsl:value-of    select="local-name()"/>
      </xsl:attribute>
    </input>
    <xsl:copy>
      <span>
        <xsl:attribute    name="class">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </span>
    </xsl:copy>
  </xsl:template>

<!-- formating dml elements -->
  <xsl:template    match="d:br">
    <br/>
  </xsl:template>
  <xsl:template    match="d:comment">
    <span    class="comment">
      <xsl:apply-templates/>
    </span>
    <br/>
  </xsl:template>
  <xsl:template    match="d:space"> </xsl:template>

<!-- content dml elements -->
  <xsl:template    match="d:examples">
    <input    type="checkbox">
      <xsl:attribute    name="name">
        <xsl:value-of    select="local-name()"/>
      </xsl:attribute>
    </input>
    <blockquote>
      <xsl:copy>
        <span    class="examples">
          <xsl:apply-templates/>
        </span>
      </xsl:copy>
    </blockquote>
  </xsl:template>
  <xsl:template    match="d:idioms">
    <input    type="checkbox">
      <xsl:attribute    name="name">
        <xsl:value-of    select="local-name()"/>
      </xsl:attribute>
    </input>
    <blockquote>
      <xsl:copy>
        <span    class="idioms">
          <xsl:apply-templates/>
        </span>
      </xsl:copy>
    </blockquote>
  </xsl:template>
  <xsl:template    match="d:reflexie">
    <xsl:copy>
      <a    class="reflexie">
        <xsl:attribute    name="href">
          <xsl:value-of    select="@xlink:href"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </a>
    </xsl:copy>
  </xsl:template>
  <xsl:template    match="d:pronunciation">
    <xsl:if    test="text()">
      <input    type="checkbox">
        <xsl:attribute    name="name">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
      </input>
      <xsl:copy>
        <xsl:text>/</xsl:text>
        <span    class="pronunciation">
          <xsl:apply-templates/>
        </span>
        <xsl:text>/</xsl:text>
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:pos">
    <xsl:if    test="text()">
      <input    type="checkbox">
        <xsl:attribute    name="name">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
      </input>
      <xsl:copy>
        <span    class="pos">
          <xsl:apply-templates/>
        </span>
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:headword">
    <xsl:if    test="text()">
      <input    type="checkbox">
        <xsl:attribute    name="name">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
      </input>
      <xsl:copy>
        <span    class="headword">
          <xsl:apply-templates/>
        </span>
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:reading">
    <xsl:if    test="text()">
      <input    type="checkbox">
        <xsl:attribute    name="name">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
      </input>
      <xsl:copy>
        <span    class="reading">[
          <xsl:apply-templates/>]
        </span>
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:sense">
    <input    type="checkbox">
      <xsl:attribute    name="name">
        <xsl:value-of    select="local-name()"/>
      </xsl:attribute>
    </input>
    <blockquote>
      <xsl:copy>
        <span    class="sense">
          <xsl:apply-templates/>
        </span>
      </xsl:copy>
    </blockquote>
  </xsl:template>
  <xsl:template    match="d:translations">
    <input    type="checkbox">
      <xsl:attribute    name="name">
        <xsl:value-of    select="local-name()"/>
      </xsl:attribute>
    </input>
    <blockquote>
      <xsl:copy>
        <xsl:apply-templates/>
      </xsl:copy>
    </blockquote>
  </xsl:template>
  <xsl:template    match="d:writing">
    <xsl:if    test="text()">
      <input    type="checkbox">
        <xsl:attribute    name="name">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
      </input>
      <xsl:copy>
        <xsl:text>【</xsl:text>
        <span    class="writing">
          <xsl:apply-templates/>
        </span>
        <xsl:text>】</xsl:text>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

<!-- content dml elements without checkbox -->
  <xsl:template    match="d:fra|d:est|d:volume|d:contribution|d:data|d:entry|d:vedette|d:bloc-gram|d:bloc-semantique| bloc-equivalent|d:exemple|d:idioms|d:rection-equiv|d:unite-phraseol">
    <xsl:copy>
      <span>
        <xsl:attribute    name="class">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </span>
    </xsl:copy>
  </xsl:template>
  <xsl:template    name="alternated-row">
    <xsl:attribute    name="class">
      <xsl:if    test="position() mod 2 = 1">oddrow</xsl:if>
      <xsl:if    test="position() mod 2 = 0">evenrow</xsl:if>
    </xsl:attribute>
  </xsl:template>
</xsl:stylesheet>
