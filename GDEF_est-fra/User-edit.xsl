<xsl:stylesheet
   xmlns="http://www.w3.org/1999/xhtml"
  xmlns:d="http://www-clips.imag.fr/geta/services/dml"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xml="http://www.w3.org/XML/1998/namespace"    exclude-result-prefixes="d xlink xsl xsi"    version="1.0">
  <xsl:output    encoding="utf-8"    indent="yes"    method="html"/>

<!-- Copy through any element that doesn't have a template. -->

<!-- Include attributes and content. -->
  <xsl:template    match="*|@*"    name="notemplate"    priority="-2">
    <xsl:copy>
      <xsl:apply-templates    select="@*|*|text()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template    match="comment()"    name="comment">
    <xsl:comment>
      <xsl:value-of    select="."/>
    </xsl:comment>
  </xsl:template>

<!-- transform any dml element into a span -->
  <xsl:template    match="d:*"    name="dml"    priority="-1">
    <xsl:copy>
      <span>
        <xsl:attribute    name="class">
          <xsl:value-of    select="local-name()"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </span>
    </xsl:copy>
  </xsl:template>

<!-- automatically generated templates -->
  <xsl:template    match="d:formes">
    <span    style="color: green;">
      <xsl:apply-templates/>
    </span>
  </xsl:template>
</xsl:stylesheet>
