<xsl:stylesheet
  
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xml="http://www.w3.org/XML/1998/namespace"    version="1.0">
  <xsl:output    encoding="UTF-8"    indent="no"    method="html"    standalone="yes"/>

<!-- Feuille de style pour transformer les documents xml en html avec des couleurs For HTML spaces, I did not find a good solution. I can't use the feature disable-output-escaping="yes" so I cheated and added a dummy css style all white for the <span class="space"> class I would like to find a solution in genreal for HTML entities... -->

<!-- root element -->

<!-- xsl:template match="/"> <div><xsl:apply-templates/></div> </xsl:template -->

<!-- root element -->
  <xsl:template    match="/">
    <xsl:variable    name="actualnamespaces">
      <xsl:for-each    select="namespace::*">
        <xsl:value-of    select="name()"/>,
      </xsl:for-each>
    </xsl:variable>
    <html    xml:lang="fr">
      <head>
        <meta    content="text/html; charset=utf-8"    http-equiv="content-type"/>
      </head>
      <body>
        <xsl:apply-templates    select="*"/>
      </body>
    </html>
  </xsl:template>

<!-- comments -->
  <xsl:template    match="comment()">
    <br/>
    <br/>
    <span    class="xmlcomment"><!--
      <xsl:value-of    select="normalize-space()"/>-->
    </span>
  </xsl:template>

<!-- elements -->
  <xsl:template    match="*"    name="generic">
    <xsl:param    name="namespaces"/>
    <xsl:variable    name="actualnamespaces">
      <xsl:for-each    select="namespace::*">
        <xsl:value-of    select="name()"/>,
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable    name="newnamespaces">
      <xsl:if    test="$namespaces=''">
        <xsl:value-of    select="$actualnamespaces"/>
      </xsl:if>
      <xsl:if    test="$namespaces!=''">
        <xsl:value-of    select="substring($actualnamespaces,string-length($namespaces))"/>
      </xsl:if>
    </xsl:variable>
    <br/>
    <xsl:for-each    select="ancestor::*">  </xsl:for-each>
    <span    class="xmlcar">&lt;</span>
    <xsl:if    test="substring-before(name(),concat(':',local-name()))!=''">
      <span    class="xmlnsprefix">
        <xsl:value-of    select="substring-before(name(),concat(':',local-name()))"/>
      </span>
      <xsl:if    test="substring-before(name(),concat(':',local-name()))!=''">
        <span    class="xmlcar">:</span>
      </xsl:if>
    </xsl:if>
    <span    class="xmlelement">
      <xsl:value-of    select="local-name()"/>
    </span>
    <xsl:if    test="count(ancestor::*)=0">
      <xsl:call-template    name="print-default-namespace"/>
    </xsl:if>
    <xsl:call-template    name="print-namespaces">
      <xsl:with-param    name="namespaces"    select="$newnamespaces"/>
      <xsl:with-param    name="ancestors"    select="ancestor::*"/>
    </xsl:call-template>
    <xsl:apply-templates    select="@*"/>
    <xsl:if    test="*|text()">
      <span    class="xmlcar">></span>
    </xsl:if>
    <xsl:if    test="not(*|text())">
      <span    class="xmlcar">/></span>
    </xsl:if>
    <xsl:apply-templates    select="*|text()|comment()">
      <xsl:with-param    name="namespaces"    select="$actualnamespaces"/>
    </xsl:apply-templates>
    <xsl:if    test="*|text()">
      <xsl:if    test="*">
        <br/>
        <xsl:for-each    select="ancestor::*">  </xsl:for-each>
      </xsl:if>
      <span    class="xmlcar">&lt;/</span>
      <xsl:if    test="substring-before(name(),concat(':',local-name()))!=''">
        <span    class="xmlnsprefix">
          <xsl:value-of    select="substring-before(name(),concat(':',local-name()))"/>
        </span>
        <xsl:if    test="substring-before(name(),concat(':',local-name()))!=''">
          <span    class="xmlcar">:</span>
        </xsl:if>
      </xsl:if>
      <span    class="xmlelement">
        <xsl:value-of    select="local-name()"/>
      </span>
      <span    class="xmlcar">></span>
    </xsl:if>
  </xsl:template>

<!-- attributes -->
  <xsl:template    match="@*">  

<!-- xmlnsprefix -->
    <span    class="xmlnsprefix">
      <xsl:value-of    select="substring-before(name(),concat(':',local-name()))"/>
    </span>

<!-- les : eventuels -->
    <xsl:if    test="substring-before(name(),concat(':',local-name()))!=''">
      <span    class="xmlcar">:</span>
    </xsl:if>
    <span    class="xmlattribute">
      <xsl:value-of    select="local-name(current())"/>
      <span    class="xmlcar">="</span>
      <span    class="xmlvalue">
        <xsl:value-of    select="current()"/>
      </span>
      <span    class="xmlcar">"</span>
    </span>
  </xsl:template>

<!-- text -->
  <xsl:template    match="text()">
    <xsl:if    test="normalize-space()!=''">
      <span    class="xmltext">
        <xsl:value-of    select="normalize-space()"/>
      </span>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>

<!-- namespaces for the root element -->
  <xsl:template    name="print-namespaces">
    <xsl:param    name="namespaces">xml,</xsl:param>
    <xsl:param    name="ancestors"/>
    <xsl:for-each    select="namespace::*">
      <xsl:variable    name="namespace">
        <xsl:value-of    select="name()"/>,
      </xsl:variable>
      <xsl:if    test="contains($namespaces,$namespace)">
        <xsl:if    test="name()!=''">
          <br/>
          <xsl:for-each    select="ancestor::*">  </xsl:for-each>
          <span    class="xmlnsprefix">xmlns</span>
          <span    class="xmlcar">:</span>
          <span    class="xmlnsprefix">
            <xsl:value-of    select="name()"/>
          </span>
          <span    class="xmlcar">="</span>
          <span    class="xmlnsuri">
            <xsl:value-of    select="current()"/>
          </span>
          <span    class="xmlcar">"</span>
        </xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

<!-- namespaces for the root element -->
  <xsl:template    name="print-default-namespace">
    <br/>  
    <xsl:for-each    select="namespace::*">
      <xsl:if    test="name()=''">
        <span    class="xmlnsprefix">xmlns</span>
        <span    class="xmlcar">="</span>
        <span    class="xmlnsuri">
          <xsl:value-of    select="current()"/>
        </span>
        <span    class="xmlcar">"</span>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
