<xsl:stylesheet
   xmlns="http://www.w3.org/1999/xhtml"
  xmlns:d="http://www-clips.imag.fr/geta/services/dml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xml="http://www.w3.org/XML/1998/namespace"    version="1.0">
  <xsl:output    encoding="utf-8"    indent="no"    method="xml"/>

<!-- general templates please do not modify -->

<!-- here I do not use the xsl:copy because it copies also xmlns: attributes -->
  <xsl:template    match="*"    priority="-1">
    <xsl:element    name="{name()}">
      <xsl:apply-templates    select="@*|*|text()"/>
    </xsl:element>
  </xsl:template>
  <xsl:template    match="@*">
    <xsl:copy/>
  </xsl:template>
  <xsl:template    match="comment()">
    <xsl:comment>
      <xsl:value-of    select="."/>
    </xsl:comment>
  </xsl:template>
  <xsl:template    match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:status"/>

<!-- volume specific templates -->
  <xsl:template    match="d:article">
    <d:entry>
      <xsl:apply-templates/>
    </d:entry>
  </xsl:template>
  <xsl:template    match="d:historique"/>
  <xsl:template    match="d:vedette">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template    match="d:h-aspire">
    <xsl:if    test="text()='true'">
      <xsl:text>*</xsl:text>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:mot">
    <d:fra>
      <d:headword>
        <xsl:apply-templates/>
      </d:headword>
    </d:fra>
  </xsl:template>
  <xsl:template    match="d:prononciation">
    <d:pronunciation>
      <xsl:apply-templates/>
    </d:pronunciation>
  </xsl:template>
  <xsl:template    match="d:grammaire">
    <xsl:apply-templates    select="d:cat-gram"/>
    <xsl:apply-templates    select="d:genre-nbr"/>
    <xsl:if    test="d:pluriel-irr/text()!='' or d:feminin-irr/text()!=''">
      <xsl:text>(</xsl:text>
      <xsl:apply-templates    select="d:pluriel-irr"/>
      <xsl:if    test="d:pluriel-irr/text()!='' and d:feminin-irr/text()!=''">
        <xsl:text>,</xsl:text>
      </xsl:if>
      <xsl:apply-templates    select="d:feminin-irr"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:apply-templates    select="d:conjugaison"/>
    <xsl:apply-templates    select="d:aux-etre"/>
  </xsl:template>
  <xsl:template    match="d:genre-nbr">
    <xsl:if    test="text()!=''">
      <d:pos>
        <xsl:apply-templates/>
      </d:pos>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:pluriel-irr">
    <xsl:if    test="text()!=''">
      <xsl:text>pl.</xsl:text>
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:feminin-irr">
    <xsl:if    test="text()!=''">
      <xsl:text>f.</xsl:text>
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:conjugaison">
    <xsl:if    test="text()!=''">
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>
  <xsl:template    match="d:aux-etre">
    <xsl:if    test="text()='true'">
      <i>
        <xsl:text>ê</xsl:text>
      </i>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
