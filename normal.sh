#!/bin/sh
rm input/run.in
cp input/run.in.orig input/run.in
rm src/fr/imag/clips/papillon/Papillon.java
cp src/fr/imag/clips/papillon/Papillon.java.orig src/fr/imag/clips/papillon/Papillon.java
rm src/fr/imag/clips/papillon/presentation/PapillonBasePO.java
cp src/fr/imag/clips/papillon/presentation/PapillonBasePO.java.orig src/fr/imag/clips/papillon/presentation/PapillonBasePO.java
rm src/fr/imag/clips/papillon/presentation/LoginUser.java
cp src/fr/imag/clips/papillon/presentation/LoginUser.java.orig src/fr/imag/clips/papillon/presentation/LoginUser.java
rm src/fr/imag/clips/papillon/presentation/EditEntry.java
cp src/fr/imag/clips/papillon/presentation/EditEntry.java.orig src/fr/imag/clips/papillon/presentation/EditEntry.java
rm src/fr/imag/clips/papillon/presentation/EditEntryInit.java
cp src/fr/imag/clips/papillon/presentation/EditEntryInit.java.orig src/fr/imag/clips/papillon/presentation/EditEntryInit.java
rm src/fr/imag/clips/papillon/presentation/AdminUsers.java
cp src/fr/imag/clips/papillon/presentation/AdminUsers.java.orig src/fr/imag/clips/papillon/presentation/AdminUsers.java
rm src/fr/imag/clips/papillon/presentation/AdminContributions.java
cp src/fr/imag/clips/papillon/presentation/AdminContributions.java.orig src/fr/imag/clips/papillon/presentation/AdminContributions.java
rm src/fr/imag/clips/papillon/presentation/ReviewContributions.java
cp src/fr/imag/clips/papillon/presentation/ReviewContributions.java.orig src/fr/imag/clips/papillon/presentation/ReviewContributions.java
echo "installation normale finie"
