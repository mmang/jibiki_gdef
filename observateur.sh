#!/bin/sh
rm input/run.in
cp input/run.in.observateur input/run.in
rm output/config.xml
cp input/config.xml output/config.xml
rm src/fr/imag/clips/papillon/Papillon.java
cp src/fr/imag/clips/papillon/Papillon.java.observateur src/fr/imag/clips/papillon/Papillon.java
rm src/fr/imag/clips/papillon/presentation/PapillonBasePO.java
cp src/fr/imag/clips/papillon/presentation/PapillonBasePO.java.observateur src/fr/imag/clips/papillon/presentation/PapillonBasePO.java
rm src/fr/imag/clips/papillon/presentation/LoginUser.java
cp src/fr/imag/clips/papillon/presentation/LoginUser.java.observateur src/fr/imag/clips/papillon/presentation/LoginUser.java
rm src/fr/imag/clips/papillon/presentation/EditEntry.java
cp src/fr/imag/clips/papillon/presentation/EditEntry.java.observateur src/fr/imag/clips/papillon/presentation/EditEntry.java
rm src/fr/imag/clips/papillon/presentation/EditEntryInit.java
cp src/fr/imag/clips/papillon/presentation/EditEntryInit.java.observateur src/fr/imag/clips/papillon/presentation/EditEntryInit.java
rm src/fr/imag/clips/papillon/presentation/AdminContributions.java
cp src/fr/imag/clips/papillon/presentation/AdminContributions.java.observateur src/fr/imag/clips/papillon/presentation/AdminContributions.java
rm src/fr/imag/clips/papillon/presentation/AdminUsers.java
cp src/fr/imag/clips/papillon/presentation/AdminUsers.java.observateur src/fr/imag/clips/papillon/presentation/AdminUsers.java
rm src/fr/imag/clips/papillon/presentation/ReviewContributions.java
cp src/fr/imag/clips/papillon/presentation/ReviewContributions.java.observateur src/fr/imag/clips/papillon/presentation/ReviewContributions.java
echo "installation observateur finie"
